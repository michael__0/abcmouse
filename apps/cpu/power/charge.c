#include "charge.h"
#include "power.h"
#include "sdk_cfg.h"
#include "rtc/rtc_api.h"
#include "led/led.h"
/* #include "sys_detect.h" */
#include "dac.h"
#include "key_drv/key.h"
#include "audio/dac_api.h"
#include "clock.h"
#include "timer.h"
#include "adc_api.h"
#include "irq_api.h"
#include "clock_api.h"
#include "power_manage_api.h"

#include "warning_tone.h"
#include "task_manager.h"
#include "task_idle.h"
#include "msg.h"
//该宏配置是否开着机充电，当需要保持开机状态充电时，定义该宏
/* #define POWER_ON_CHARGE */

/* #define CHARGEE_DBG */
#ifdef  CHARGEE_DBG
#define charge_putchar        putchar
#define charge_printf         log_printf
#define charge_buf            printf_buf
#else
#define charge_putchar(...)
#define charge_printf(...)
#define charge_buf(...)
#endif    //CHARGEE_DBG

static void delay_ms()
{
    //Timer2 for delay
    JL_TIMER2->CON = BIT(14);
    JL_TIMER2->PRD = 375;
    JL_TIMER2->CNT = 0;
    SFR(JL_TIMER2->CON, 2, 2, 2); //use osc
    SFR(JL_TIMER2->CON, 4, 4, 3); //div64
    SFR(JL_TIMER2->CON, 14, 1, 1); //clr pending
    SFR(JL_TIMER2->CON, 0, 2, 1); //work mode
    while (!(JL_TIMER2->CON & BIT(15)));
    JL_TIMER2->CON = BIT(14);
}

void delay_nms(u32 n)
{
    while (n--) {
        delay_ms();
    }
}


/*
void charge_power_on_detect_deal(void)//开机检测是否插入了充电线
{
    CHE_PIN_IN();
    charge_led_init();

    while (1)
    {
        clear_wdt();
        delay(30000);

        if(!CHE_PIN_STATE())//充电不开机
        {
            log_printf("chargeing not to turn on......\n");
            charge_led_on();
            enter_sys_soft_poweroff();
        }
        else
        {
            log_printf("no chargeing ......\n");
            break;
        }
    }
}
*/

static  u8 battery_charging_cnt = 0;
static  u8 battery_chargingfull_cnt = 0;
static  u8 battery_nocharging_cnt = 0;
volatile u8 battery_charge_flag = 0;


u8 get_battery_charge_flag(void)
{
    return battery_charge_flag;
}


void battery_charging_check(void)//正常工作后,检测充电
{

        if(!CHG_FINISH_STATE())
        {
            battery_chargingfull_cnt++;
        }
        else if(!CHG_IN_STATE())
        {
           battery_charging_cnt++;
        }
        else
        {
            battery_nocharging_cnt++;
        }



       if(battery_charging_cnt >= 4)
       {
            //log_printf("charging !!!!!!!!!!!!!!!!\n");

            battery_charge_flag = 1;
            led_fre_set(C_RLED_ON_MODE);
            //task_post_msg(NULL, 1, MSG_TURN_TO_IDLE);

            battery_charging_cnt     = 0;
            battery_chargingfull_cnt = 0;
            battery_nocharging_cnt   = 0;
       }
       else if(battery_chargingfull_cnt >= 4)
       {
            //log_printf("charge full !!!!!!!!!!!!!!!!\n");

            battery_charge_flag = 1;
//            led_fre_set(C_GLED_ON_MODE);
            //task_post_msg(NULL, 1, MSG_TURN_TO_IDLE);

            battery_charging_cnt     = 0;
            battery_chargingfull_cnt = 0;
            battery_nocharging_cnt   = 0;
       }
       else if(battery_nocharging_cnt >= 4)
       {
            //log_printf("no charge !!!!!!!!!!!!!!!!\n");
            if(battery_charge_flag)
            {
                POWER_KEEP_OFF();
 //               SPEAKER_DECODER_LOW();
                enter_sys_soft_poweroff();
            }

            battery_charging_cnt     = 0;
            battery_chargingfull_cnt = 0;
            battery_nocharging_cnt   = 0;
       }

}

/*
LOOP_DETECT_REGISTER(charge_detect) = {
    .time = 125, //250ms
    .fun  = battery_charging_check,
};
*/
