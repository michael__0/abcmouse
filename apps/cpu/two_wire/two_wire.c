#include "uart.h"
#include "two_wire.h"
#include "sdk_cfg.h"
#include "led.h"
#include "task_manager.h"
#include "timer.h"
#include "fs_io.h"
#include "file_op_err.h"
#include "file_operate.h"
#include "dev_manage.h"
#include "ff_api.h"
#include "music_player.h"


/*
master ---- DSP/MCU
slave ----- OID decoder

*/

/*
Default : SCK is kept low by Master, and SDIO is released and pulled high by
external pull-up resister.
*/
#define TW_TAG os_time_get()*10,"TW"
#define DEBOUNCE_TIMES 3
static u32 oidcode = 0xFFFFFFFF;
static u32 last_oidcode = 0xFFFFFFFF;
static u8 reading_state = 0;
u8 timer_enable = 0;

#if 0
void clear_oidcode(void);

static void master_init()
{
    tw_clk_out();
    tw_clk_l();

    tw_data_in(); //ready to rec data
    tw_delay();
}


/*
Master initiates a transfer cycle by changing SCK from low to high.
*/
static void master_send_start()
{
    tw_clk_l();
    tw_delay();
    tw_clk_h();
    tw_delay();
}


/*
If Master keeps low on SCK over 78¦Ìs.
Slave supposes that the transfer cycle is finished.
*/
static void master_send_end()
{
    tw_clk_out();
    tw_data_in();//release sdio
    tw_clk_l();
    delay(1200);
}


/*
Before Read cycle, Slave generates transfer request (pulling low SDIO line) to inform
Master.
*/
static bool master_check_revstate()
{
    tw_data_in();
    tw_clk_l();

    delay(1000);
    if(!tw_data_r())//data low
    {
        return TRUE;
    }
    else
        return FALSE;
}


static u16 master_wait_ack()
{
	u16 i = 0, receive_dat = 0, count = 0;

    if(master_check_revstate() == FALSE)//no ACK
        return receive_dat;

    master_send_start();

    tw_data_out(); // read control bit
    tw_delay();
    tw_data_l();
    tw_delay();

    tw_clk_l();    //positive edge
    tw_delay();
    tw_clk_h();
    tw_delay();

    tw_data_in();
    tw_delay();

	for(i=0; i<16; i++)
	{
	    tw_clk_h();
        tw_delay();

        tw_clk_l();
        tw_delay();

        receive_dat <<= 1;
        if(tw_data_r())
            receive_dat |= 0x01;


	}

    master_send_end();

//    log_printf("%d\t\t%s\t\twait ACK : 0x%x\n",TW_TAG, receive_dat);
	return receive_dat;
}



static void master_send_onebit(bit1 bit)
{
    tw_data_out();
    tw_delay();

    if(bit)
        tw_data_h();
    else
        tw_data_l();

    tw_clk_h();
    tw_delay();
    tw_clk_l();
    tw_delay();

}


static void master_send_onebyte(u8 data)
{
    log_printf("%d\t\t%s\t\tmaster_send_one byte start:0x%02x\n",TW_TAG, data);

    u8 temp = data, count = 0;

    while(count++ <10)//SDIO should be checked before every Write cycle.
    {
        if(master_check_revstate() == FALSE)
            break;
        else
        {
            master_send_end(); //end signal
            log_printf("%d\t\t%s\t\tstop read.\n",TW_TAG);
        }
    }

    if(count==10) //not write.
        return;


    master_send_start(); //start signal

    master_send_onebit(1);//write control bit 1

    for(int i=0; i<8; i++)                //write one byte
    {
        if((data & 0x80)>0)               //MSB bit
           master_send_onebit(1);         //pull high data IO
        else
           master_send_onebit(0);         //pull low data IO

        data <<= 1;
    }

    master_send_end();                    //end signal

    count = 0;
    while(count++ < 200)
    {
        if(master_wait_ack() == ((u16)temp+1) )    //check send is valid.
        {
            log_printf("%d\t\t%s\t\tsend valid.\n",TW_TAG);
            break;
        }
        else
        {
            log_printf("%d\t\t%s\t\tsend invalid, count: %d\n",TW_TAG, count);
            tw_delay();
        }
    }

    log_printf("%d\t\t%s\t\tmaster_send_one byte  end.............\n",TW_TAG);
    return  ;
}

/*
Multi-Write Command
*/
static void master_send_nbyte(u8 *cmd, u8 len)
{
    if(!cmd || len == 0)
        return ;

    while (len--)
    {
        master_send_onebyte(*cmd++);
        delay_n10ms(30);//between two commands must > 250ms
    }

    return ;
}


u8 master_readfrom_slave_onebyte()
{
    u8 i, receive_dat = 0;

	for(i=0; i<8; i++)
	{


        tw_clk_h();
        tw_delay();

	    tw_clk_l();
        tw_delay();

        if(tw_data_r())
            receive_dat |= (1<<(7-i));

	}


	//log_printf("%d\t\t%s\t\treceive_dat:0x%02x\n",TW_TAG, receive_dat);
    return receive_dat;
}


static void master_readfrom_slave_nbyte(u8 data[], u8 n)
{
    memset(data, 0, n);
    master_send_start(); //start signal

    tw_data_out(); // read control bit  SCK:H
    tw_data_l();
    tw_delay();

    tw_clk_l();    //positive edge
    tw_delay();

    tw_data_in();  //released SDIO
    tw_delay();

    while(n--)
    {
       *data = master_readfrom_slave_onebyte();

        data++;
    }

    master_send_end();

    return ;
}

u8 reading_flag = 0;
static u8 interval_time = 0;
static bool master_readfrom_slave(u8 *data)
{
    static u8 packet_same_times;
    static u8 data_temp1[4];
    static u8 data_temp2[4];
    static u8 data_temp3[4];
    static u8 data_temp4[4];

    static u8 led_time_cnt = 0;
        if(led_time_cnt)
        {
            led_time_cnt--;
            if((led_time_cnt==0)&&(task_get_cur() == TASK_ID_OID))//||(get_emitter_task_id()==TASK_ID_OID)))
            {
                led_fre_set(C_BLED_ON_MODE);
            }
        }

        if((master_check_revstate()==TRUE))//check rev state
        {
           master_readfrom_slave_nbyte(data,8);
           interval_time = 25;
           //log_printf("recivdata: 0x%02x, 0x%02x, 0x%02x, 0x%02x, 0x%02x, 0x%02x, 0x%02x, 0x%02x\n",TW_TAG,data[0],data[1],data[2],data[3],data[4],data[5],data[6],data[7]);

           reading_state = 1;

           if((!(data[0]&BIT(4)))&&(data[0]!=0))
           {
               //log_printf("recivdata: 0x%02x, 0x%02x, 0x%02x, 0x%02x, 0x%02x, 0x%02x, 0x%02x, 0x%02x\n",TW_TAG,data[0],data[1],data[2],data[3],data[4],data[5],data[6],data[7]);

               if(packet_same_times ==1)
               {

                    if((0==memcmp(data_temp1, data_temp2, 4)) && (0!=memcmp(data_temp2, &data[4], 4))&& (0!=memcmp(data_temp3, &data[4], 4)))
                    {
                                                memcpy(data_temp3, &data[4], 4);
                    }
                    else if((0==memcmp(data_temp1, data_temp2, 4)) && (0==memcmp(data_temp2, &data[4], 4)))
                   {
                                                memset(data_temp3, 0x33, 4);
                                                memset(data_temp4, 0x44, 4);
                    }

                    else if((0==memcmp(data_temp3, &data[4], 4))&&(0!=memcmp(data_temp2, &data[4], 4))&& (0!=memcmp(data_temp3, data_temp4, 4)))
                    {
                                                memcpy(data_temp4, &data[4], 4);
                    }

                    else if((0==memcmp(data_temp3, data_temp4, 4)) && (0!=memcmp(data_temp4, &data[4], 4)))
                    {
                        memset(data_temp3, 0x33, 4);
                        memset(data_temp4, 0x44, 4);
                    }
                    else if((0==memcmp(data_temp3, data_temp4, 4)) && (0==memcmp(data_temp4, &data[4], 4)) && (0!=memcmp(data_temp2, data_temp4, 4)))
                    {
                        if(packet_same_times == 1)
                        {
                              packet_same_times =1;
                              led_time_cnt = 50;
                              reading_flag = 1;

                            memcpy(data_temp1, &data[4], 4);
                            memcpy(data_temp2, &data[4], 4);

                            memset(data_temp3, 0x33, 4);
                            memset(data_temp4, 0x44, 4);

                              return TRUE;
                        }
                    }
               }

               else
                if((0!=memcmp(data_temp1, &data[4], 4)) && (0!=memcmp(data_temp2, &data[4], 4))&& (0!=memcmp(data_temp2, data_temp1, 4)))
                {
                    memcpy(data_temp1,&data[4], 4);
                }
                else if((0==memcmp(data_temp1, &data[4], 4)) && (0!=memcmp(data_temp2, &data[4], 4))&&(0!=memcmp(data_temp2, data_temp1, 4)))
                {
                    memcpy(data_temp2,&data[4], 4);
                }
                else if((0==memcmp(data_temp1, data_temp2, 4)) && (0==memcmp(data_temp2, &data[4], 4)))
                {
                    if(packet_same_times == 0)
                    {
                          packet_same_times =1;
                          led_time_cnt = 50;
                          reading_flag = 1;
                          return TRUE;
                    }
                }
           }
            else
            {
                return FALSE;
            }
        }
        else
        {
            if(interval_time == 0)
            {
                reading_state = 0;
                memset(data_temp1, 0x11, 4);
                memset(data_temp2, 0x22, 4);
                memset(data_temp3, 0x33, 4);
                memset(data_temp4, 0x44, 4);
                packet_same_times = 0;
                clear_oidcode();
            }
            else
                interval_time--;

            return FALSE;
        }
        return FALSE;
}


static u32 master_parse_receive_data(u8 *data)
{
    return ((u32)data[4]<<24)|((u32)data[5]<<16)|((u32)data[6]<<8)|((u32)data[7]);
}

static u32 master_receive_end_parse_data(void)
{
    u8 data[8]={0};

    if(master_readfrom_slave(data))
    {
        return master_parse_receive_data(data);
    }

    return 0;
}

void set_oidcode(u32 code)
{

    interval_time = 25;

    oidcode =  code;


    if(task_get_cur() == TASK_ID_OID)
    {
        led_fre_set(C_BLED_OFF_MODE);
    }

    extern void reset_poweroff_time(void);
    reset_poweroff_time();

    log_printf("%d\t\t%s\t\tset_oidcode:0x%08X\n",TW_TAG, oidcode);
}

void clear_oidcode(void)
{
    oidcode = 0xFFFFFFFF;
}

u32 get_oidcode(void)
{
    u32 ret_oidcode;

    ret_oidcode = oidcode;
    oidcode = 0xFFFFFFFF;
    //log_printf("%d\t\t%s\t\tget_oidcode :%d\n",TW_TAG, ret_oidcode);
    return ret_oidcode;
}

u8 get_readflag(void)
{
    u32 ret_reading_flag;

    ret_reading_flag = reading_flag;
    reading_flag = 0;

    return ret_reading_flag;
}

u8 Write_Register(u16 Data1,u16 Data2)
{
  //Cmd: 0x73, Data1: Register Address, Data2: Register Value
  u8 Ret,i;
  u8 ErrorCount = 0;
  u8 TransCmd[7];
  Ret = i = 0;

  TransCmd[0] = 0X73;
  TransCmd[1] = 0x05;
  TransCmd[2] = (Data1>>8);
  TransCmd[3] = Data1;
  TransCmd[4] = (Data2>>8);
  TransCmd[5] = Data2;
  TransCmd[6] = (TransCmd[1] + TransCmd[2] + TransCmd[3] + TransCmd[4] + TransCmd[5]);

  for(i=0;i<7;i++)
  {
    //TransmitCmd(TransCmd[i]);
    master_send_onebyte(TransCmd[i]);
  }

  return Ret;
}

// Setup decoder SOC from DSP/MCU
void master_setup_slave_init()
{
    u8 data[8] = {0}, count= 0;
    u8 init[2] = {0x27, 0x40};
    //exit configuration mode end enter to normal mode
    tw_clk_out();
    tw_clk_h();
    delay_n10ms(5);                         //over 20ms and less than 2sec

    master_init();                          //Default state
    log_printf("%d\t\t%s\t\t---------------master_setup_slave_init------------------\n",TW_TAG);


    while(count < 100)                      //check Decoder SOC is already entered to normal mode.
    {
        log_printf("%d\t\t%s\t\tcount:%d\n",TW_TAG, count);
        if(master_check_revstate())         //check rev state
        {
            memset(data, 0, sizeof(data));
            master_readfrom_slave_nbyte(data,8);
            log_printf("%d\t\t%s\t\tmaster_readfrom_slave_nbyte:0x%02x,0x%02x,0x%02x,0x%02x,0x%02x,0x%02x,0x%02x,0x%02x\n",TW_TAG,data[0],data[1],data[2],data[3],data[4],data[5],data[6],data[7]);

            if(data[6]==0xFF && data[7]==0xF8)
                break;
            else if(data[6]==0xFF && data[7]==0xF6)//check oidpen head.
            {
                tw_data_in();
                tw_clk_in();
                while(1);
            }
        }
        delay_2ms(1);
        count++;
    }

    if(count == 100)
    {
        log_printf("%d\t\t%s\t\t\n\n\n\n\n\n\n---------------init two wire fail------------------\n\n\n\n\n\n",TW_TAG);
    }
    else
    {
        log_printf("%d\t\t%s\t\tDecoder SOC has already entered to normal mode.\n\n\n",TW_TAG);
        master_send_nbyte(init, sizeof(init));
    }
    timer_enable = 1;

}

u8 get_read_state(void)
{
    return  reading_state;
}


void master_receive_oidcode(void)
{
    u8 data[8]={0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF};
    u32  code;
    if(timer_enable ==0)
    {
        return;
    }
    if(master_readfrom_slave(data))
    {
        code = ((u32)data[4]<<24)|((u32)data[5]<<16)|((u32)data[6]<<8)|((u32)data[7]);
        set_oidcode(code);
    }
}
#endif
//=======================================================================================//
int cmd_buf[3] = {0x02AA,0x0D21,0x1011};
int dpr_close[3] = {0xE2F, 0x03,0x01};
int serial_buf[3] = {0x02AA, 0x0D5F, 0x0001};
int cmd_20fps[3] = {0x0e02, 0x0000 ,0x0002};
int sleep_disable = 0xA3;                           //disable 5min sleep function
int restart_cmd = 0x63;                             //reset decoder
int cali_cmd[3] = {0x0501, 0x00C8, 0x0280};
int s2_all[3] = {0x0e36, 0x000F, 0x0000};
int more_parse[3] = {0x02AA,0x1840,0x0A00};			//ACK:0x70001

uint32_t cali_para1 = 0xA69;
uint32_t cali_para2 = 0x7AA;

const char s2_mapping[]="/mapping.bin";
static void bit2bit(uint32_t data,uint32_t *buf,uint32_t d_bit,uint32_t b_bit);

typedef struct aio
{
    char header;
    char reserver;
    uint32_t data;
    uint32_t index;
    bool stat;
    bool valid;
}aio_t;

aio_t aio_data;

static bool aio_s3_checksum(aio_t *data);

static void aio_to_index(aio_t *data);
#define AIO_SCL     1
#define AIO_SDA     2
void aio_set_io_output(int gpio)
{
    if(gpio == AIO_SCL)
    {
        tw_clk_out();
    }
    else if(gpio == AIO_SDA)
    {
        tw_data_out();
    }
    else
    {
        log_printf("ERROR: aio set io output funciton,the io %d is error!\n",gpio);
    }
}

void aio_set_io_level(int gpio,bool level)
{
    if(gpio==AIO_SCL)
    {
        if(level)
        {
            tw_clk_h();
        }
        else
        {
            tw_clk_l();
        }
    }
    else if(gpio == AIO_SDA)
    {
        if(level)
        {
            tw_data_h();
        }
        else
        {
            tw_data_l();
        }
    }
    else
    {
        log_printf("ERROR: aio set io level funciton,the io %d is error!\n",gpio);

    }
}
//=======================================================================//
void aio_gpio_init(void)
{
    tw_clk_out();
    tw_data_in();
}
//=======================================================================//
void aio_set_io_input(gpio)
{
    if(gpio == AIO_SDA)
    {
        tw_data_in();
    }
    else
    {
        log_printf("aio set io input gpio num error:%d\n",gpio);
    }
}
//=======================================================================//
int aio_read_sda(void)
{
    if(tw_data_r())
    {
        return 1;
    }
    else
    {
        return 0;
    }
}
//==========================================================//
int aio_read(aio_t *buf)
{
    uint8_t i,j;
	uint8_t aio_len = 0;
	int len = 0;
	if(aio_read_sda()==1)
	{
		return 0;
	}
	buf->stat = true;
	buf->header = 0;
	buf->reserver = 0;
	buf->data = 0;
	buf->index = 0;
   	aio_set_io_output(AIO_SCL);
	aio_set_io_output(AIO_SDA);
	aio_set_io_level(AIO_SCL,GPIO_HIGH);
	aio_set_io_level(AIO_SDA,GPIO_LOW);
	tw_delay();
	aio_set_io_level(AIO_SCL,GPIO_LOW);
	tw_delay();
	aio_set_io_level(AIO_SCL,GPIO_HIGH);
	aio_set_io_input(AIO_SDA);
	for(i=0;i<7;i++)
	{
		aio_set_io_level(AIO_SCL,GPIO_HIGH);
		buf->header = buf->header << 1;
		tw_delay();
		aio_set_io_level(AIO_SCL,GPIO_LOW);
		if(aio_read_sda())
		{
			buf->header++;
		}
		tw_delay();
	}
	if(buf->header >= 0x61 && buf->header <= 0x63)			//45bit
	{
		for(i=0;i<6;i++)
		{
			aio_set_io_level(AIO_SCL,GPIO_HIGH);
			buf->reserver = buf->reserver << 1;
			tw_delay();
			aio_set_io_level(AIO_SCL,GPIO_LOW);
			if(aio_read_sda())
			{
				buf->reserver++;
			}
			tw_delay();
		}
		len = 47;
		aio_len = 34;
	}
	else if(buf->header == 0x69)							//pen list signal
	{
		aio_len = 9;
		len =  16;
	}
	else if(buf->header == 0x70)							//calibration
	{
		aio_len = 16;
		len = 23;
	}
	else
	{
		aio_len = 16;
		len = 23;
	}
	i=j=0;
	while(1)
	{
		aio_set_io_level(AIO_SCL,GPIO_HIGH);
		buf->data = buf->data << 1;
		tw_delay();
		aio_set_io_level(AIO_SCL,GPIO_LOW);
		if(aio_read_sda())
		{
			buf->data++;
		}
		i++;
		if(i>=8)
		{
			i=0;
			j++;
		}
		tw_delay();
		aio_len--;
		if(aio_len == 0)
		{
			delay(1200);                        //delay 80us stop the transtion;
			break;
		}
	}
    //log_printf("%d\t\t%s\t\t [*] get the aio header is 0x%x,data is 0x%x\n",TW_TAG,buf->header,buf->data);
	//aio_to_index(buf);
	return len;
}
//====================================================================//
void aio_write_cmd(int cmd)
{
    int i = 0;
	int j = 0;
	int  iTemp;
	log_printf("%d\t\t%s\t\t aio_cmd 1\n",TW_TAG);
	if(aio_read_sda()==0)
	{
		aio_read(&aio_data);
	}
	aio_set_io_output(AIO_SDA);
	aio_set_io_level(AIO_SCL,1);
	aio_set_io_level(AIO_SDA,1);
	tw_delay();
	aio_set_io_level(AIO_SCL,0);
	tw_delay();

	iTemp = cmd;
	for(j=0;j<8;j++)
	{
		aio_set_io_level(AIO_SCL,1);
		if(iTemp & 0x80)
		{
			aio_set_io_level(AIO_SDA, 1);
		}
		else
		{
			aio_set_io_level(AIO_SDA, 0);
		}
		iTemp = iTemp << 1;
		tw_delay();
		aio_set_io_level(AIO_SCL, 0);
		tw_delay();
	}
	aio_set_io_input(AIO_SDA);
	delay(1200);
	log_printf("info:write the cmd :0x%x suc\n",cmd);
}
//========================================================================//
void aio_write_data(int data1,int data2,int data3)
{
    int i = 0;
	int j = 0;
	int  iTemp;
	log_printf("%d\t\t%s\t\t info:aio init end,write cmd buf is 0x%x   0x%x   0x%x\n",TW_TAG,data1,data2,data3);
	if(aio_read_sda()==0)
	{
		aio_read(&aio_data);
	}
	aio_set_io_output(AIO_SDA);
	aio_set_io_level(AIO_SCL,1);
	aio_set_io_level(AIO_SDA,1);
	tw_delay();
	aio_set_io_level(AIO_SCL,0);
	tw_delay();

	for( i=0; i<3; i++)
	{
        if(i==0)
        {
            iTemp = data1;
        }
        else if(i==1)
        {
            iTemp = data2;
        }
        else if(i==2)
        {
            iTemp = data3;
        }
		for(j=0;j<16;j++)
		{
			aio_set_io_level(AIO_SCL,1);
			if(iTemp & 0x8000)
			{
				aio_set_io_level(AIO_SDA, 1);
			}
			else
			{
				aio_set_io_level(AIO_SDA, 0);
			}
			iTemp = iTemp << 1;
			tw_delay();
			aio_set_io_level(AIO_SCL, 0);
			tw_delay();
		}
	}
	aio_set_io_input(AIO_SDA);
	delay(1200);
	i = 200;
	while(i)
	{
		delay_2ms(1);
		if(aio_read_sda()==0)
		{
			aio_read(&aio_data);
			log_printf("%d\t\t%s\t\t info:ack header is 0x%x,reserver is 0x%x,data is 0x%x,index is 0x%x\n",TW_TAG, aio_data.header, aio_data.reserver, aio_data.data, aio_data.index);
			break;
		}
		i--;
	}
}

//====================================================================//
void aio_cali(uint32_t para1,uint32_t para2)
{
    int buf[3];
	buf[0] = 0x0101;
	buf[1] = para1 & 0x0000FFFF;
	buf[2] = (para1 & 0x00030000) > 16;
    printf("aio_cali 1 write data is 0x%x,0x%x,0x%x\n",buf[0],buf[1],buf[2]);
	aio_write_data(buf[0],buf[1],buf[2]);

	buf[0] = 0x0201;
	buf[1] = para2 & 0x0000FFFF;
	buf[2] = (para2 & 0x00030000) > 16;
    printf("aio_cali 2 write data is 0x%x,0x%x,0x%x\n",buf[0],buf[1],buf[2]);
	aio_write_data(buf[0],buf[1],buf[2]);
}

bool aio_init(void)
{
    int iTemp;
	int ret;
	int cnt = 0;
	log_printf("%d\t\t%s\t\tinfo:aio init start\n",TW_TAG);
	aio_gpio_init();

	uint16_t delay_times = 100;

	aio_set_io_level(AIO_SCL,GPIO_HIGH);
	delay_n10ms(5);
	aio_set_io_level(AIO_SCL,GPIO_LOW);
	while(delay_times)
	{
		delay_times--;
		delay_2ms(1);
		if(aio_read_sda() == 0)
		{
			log_printf("%d\t\t%s\t\tinfo：aio_init:aio read!\n",TW_TAG);
			aio_read(&aio_data);
			break;
		}
		if(delay_times % 100 == 0)
		{
			iTemp = aio_read_sda();
		}
	}
	log_printf("%d\t\t%s\t\tinfo:aio_init:aio read! head is 0x%x,data is 0x%x\n",TW_TAG,aio_data.header,aio_data.data);
	if(aio_data.header == 0x60)
	{
		if(aio_data.data== 0xFFF8)
		{
			#ifdef CALI_ENABLE
			aio_write_data(cali_cmd[0],cali_cmd[1],cali_cmd[2]);
			cnt = 0;
			while(1)
			{
				delay_n10ms(1);
				if(aio_read_sda()==0)
				{
					ret = aio_read(&aio_data);
					log_printf("info:ack header is 0x%x,reserver is 0x%x,data is 0x%x,index is 0x%x\n",aio_data.header, aio_data.reserver, aio_data.data, aio_data.index);
					//log_printf("ret = %d\n",ret);
					if(ret)
					{
						cnt++;
						//log_printf("cnt = %d\n",cnt);
						if(cnt > 2)
							break;
					}

				}
			}
			aio_write_cmd(0x63);
			#else

			aio_cali(cali_para1,cali_para2);
			aio_write_data(dpr_close[0],dpr_close[1],dpr_close[2]);
			aio_write_data(cmd_buf[0],cmd_buf[1],cmd_buf[2]);
			//aio_write_data(cmd_20fps[0], cmd_20fps[1],cmd_20fps[2]);
			//aio_write_data(more_parse[0], more_parse[1], more_parse[2]);

			aio_write_cmd(0xA3);

			#endif
			return 0;
		}
		else
		{
			return 1;
		}
	}
	return 2;
}
//=========================================================================//
void aio_app(void)
{
    uint32_t tmp1 = 0;
	uint32_t tmp2 = 0;
    //aio_read(&aio_data);
    if(aio_read(&aio_data))
    {
        //aio_data.stat == false;
        switch(aio_data.header)
        {
            case 0x60:
                if(aio_data.data == 0xFFF8)                 //power on
                {

                }
                else if(aio_data.data == 0xFFF6)             //sleep
                {

                }
                else if(aio_data.data == 0xFFF1)            //reset
                {

                }
                else if(aio_data.data == 0xFFFC)            //paper off
                {

                }
                break;
            case 0x61:
                //oidcode = aio_data.data;
                //aio_data.valid = true;
                break;
            case 0x62:
                if(aio_s3_checksum(&aio_data)==true)
                {
                    log_printf("%d\t\t%s\t\t get valid s3 data:0x%x\n",TW_TAG,aio_data.data);
                    tmp1 = (aio_data.data >> 4);
                    tmp2 = tmp1;
                    bit2bit(tmp1,&tmp2,BIT7,BIT18);
                    bit2bit(tmp1,&tmp2,BIT12,BIT16);
                    bit2bit(tmp1,&tmp2,BIT14,BIT0);
                    bit2bit(tmp1,&tmp2,BIT10,BIT5);
                    bit2bit(tmp1,&tmp2,BIT3,BIT2);
                    if(tmp1 & BIT9)
                    {
                        tmp2 &=(~BIT9);
                    }
                    else
                    {
                        tmp2 |= BIT9;
                    }
                    aio_data.index = tmp2;
                    oidcode = aio_data.index;
                    aio_data.valid = true;
                }
                else
                {
                    aio_data.valid = false;
                }


                //log_printf("%d\t\t%s\t\t aio read header is 0x%x,data is 0x%x,index is 0x%x\n",TW_TAG,aio_data.header,aio_data.data,aio_data.index);
                break;
            case 0x63:
                oidcode = aio_data.data;
                aio_data.valid = true;
                break;
            case 0x70:
                log_printf("%d\t\t%s\t\t aio ack header is 0x%x,data is 0x%x,\n",TW_TAG,aio_data.header,aio_data.data);
                break;
            default:
                break;
        }
    }
}
//===============================================================================//
static void bit2bit(uint32_t data,uint32_t *buf,uint32_t d_bit,uint32_t b_bit)
{
  	if(data & d_bit)
	{
		*buf |= b_bit;
	}
	else
	{
		*buf &= (~b_bit);
	}
	if(data & b_bit)
	{
		*buf |= d_bit;
	}
	else
	{
		*buf &= (~d_bit);
	}
}
//========================================================================//
static void aio_to_index(aio_t *data)
{
    uint32_t tmp1 = 0;
	uint32_t tmp2 = 0;
	uint32_t data_pos = 0;
	uint32_t g_aio_index = 0;
	uint32_t data_tmp = 0;
    bool ret;
	int res;
	if(data->header == 0x61)														//to s2 index,mapping file to mapping index
	{
        log_printf("%d\t\t%s\t\t info get aio 2 index header is 0x%x,data is 0x%x\n",TW_TAG,data->header,data->data);
        //data->stat = false;
        //ret = read_s2_index(data);
	}
	else if(data->header == 0x62)													//to s3 index
	{
		aio_s3_checksum(data);

		tmp1 = (data->data >> 4);
		tmp2 = tmp1;
		bit2bit(tmp1,&tmp2,BIT7,BIT18);
		bit2bit(tmp1,&tmp2,BIT12,BIT16);
		bit2bit(tmp1,&tmp2,BIT14,BIT0);
		bit2bit(tmp1,&tmp2,BIT10,BIT5);
		bit2bit(tmp1,&tmp2,BIT3,BIT2);
		if(tmp1 & BIT9)
		{
			tmp2 &=(~BIT9);
		}
		else
		{
			tmp2 |= BIT9;
		}
		data->index = tmp2;
		log_printf( "%d\t\t%s\t\t info:the aio to index,the header is 0x%x,reserver is 0x%x,data is 0x%x,index is 0x%x\n",TW_TAG,data->header,data->reserver,data->data,data->index);
	}
	else if(data->header == 0x63)
	{
		data->index = data->data;
	}
}
//======================================================================//
static bool aio_s3_checksum(aio_t* data)
{
    char a = 0;
	char b = 0;
	char c = 0;
	char d = 0;
	int32_t temp;
	a = data->data & 0x000F;
	b = data->reserver & 0x0F;

	if(a > b)
	{
		c = a - b;
	}
	else if(a < b)
	{
		c = 16 + a - b;
	}
	else
	{
		c = 0;
	}
	temp = (data->data >> 4);
	d |=(((c >> 3) & 0x01)^((temp >> 19) & 0x01)) << 3;
	d |= (((c >> 2) & 0x01) ^ ((temp >> 22) & 0x01) ^ ((temp >> 20) & 0x01) ^ ((temp >> 23) & 0x01)) << 2;
	d |= (c & BIT1);
	d |= (c & BIT0);
	//log_printf("%d\t\t%s\t\tinfo:a = 0x%x,b = 0x%x, c = 0x%x,d = 0x%x\n",TW_TAG,a,b,c,d);
	if(d == a)
	{
		log_printf("%d\t\t%s\t\t info:checksum suc\n",TW_TAG);
		return true;
	}
	else
	{
		log_printf("%d\t\t%s\t\t error:checksum failed,the d= %d,a = %d\n",TW_TAG,d,a);
		return false;
	}
}

//========================================================================================//

u32 read_s2_index(u32 data, FILE_OPERATE* obj)
{
    u32 index = 0xFFFFFFFF;
    u32 itemp = 0;
    u32 data_pos = data *2;
    int wRet = 0;
    int err;
    log_printf("%d\t\t%s\t\t read_s2_index  ....,data is 0x%x\n",TW_TAG,data);

    _FIL_HDL *f_p = file_operate_get_file_hdl(obj);
    _FS_HDL  *fs_p = file_operate_get_fs_hdl(obj);
    wRet = fs_open(fs_p, f_p, (void *)s2_mapping, 0);
    if(wRet == 0)
    {
        index = 0;
        fs_seek(f_p, 0, data_pos);
        fs_read(f_p, (u8*)&itemp, 2);
        fs_close(f_p);
        //index |= (itemp & 0x000000FF) << 8;
        //index |= (itemp &0x0000FF00) >> 8;
        index = itemp;
        log_printf("%d\t\t%s\t\t read s2 index ,the data is 0x%x,data_pos is 0x%x,itemp is 0x%x,the index is 0x%x\n",TW_TAG, data,data_pos,itemp,index);
    }
    else
    {
        log_printf("%d\t\t%s\t\t-------open mapping.bin fail!, wRet=%d..........................\n",TW_TAG,wRet);
    }
_end:
    return index;
}

//======================================================================================//
u32 get_oidcode(void)
{
    u32 ret_oidcode = 0xFFFFFFFF;
    static u32 time_new = 0;
    static u32 time_old = 0;

    time_new = os_time_get();

    if(aio_data.valid)
    {
        if(time_new -time_old > 20)         //200ms oid send limited;
        {
            //log_printf("%d\t\t%s\t\t info: get oid code new time is %d,old time is %d\n",TW_TAG,time_new,time_old);
            ret_oidcode = oidcode;
            time_old = time_new;
        }
        else
        {
            log_printf("%d\t\t%s\t\t error: get oid code new time is %d,old time is %d\n",TW_TAG,time_new,time_old);
        }
        aio_data.valid = false;
    }
    //log_printf("%d\t\t%s\t\tget_oidcode :%d\n",TW_TAG, ret_oidcode);
    return ret_oidcode;
}
//=======================================================================================//
LOOP_DETECT_REGISTER(oidcode_detect) =
{
    .time = 5,
    //.fun  = master_receive_oidcode,
    .fun = aio_app,
};
