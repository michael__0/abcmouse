#ifndef __TASK_COMMON_H__
#define __TASK_COMMON_H__
#include "typedef.h"

tbool task_common_msg_deal(void *hdl, u32 msg);
u32 get_input_number(u32 *num);

void loop_task(void);

#define USER_VOL_5 19
#define USER_VOL_4 17
#define USER_VOL_3 15
#define USER_VOL_2 13
#define USER_VOL_1 11

#endif// __TASK_COMMON_H__
