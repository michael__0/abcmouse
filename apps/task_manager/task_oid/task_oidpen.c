#include "file_operate.h"
#include "file_op_err.h"
#include "sdk_cfg.h"
#include "task_manager.h"
#include "task_common.h"
#include "task_oidpen_key.h"
#include "two_wire.h"
#include "msg.h"
#include "warning_tone.h"
#include "music_player.h"
#include "fat_io.h"
#include "file_io.h"
#include "dev_manage.h"

#include "asm_type.h"
#include "audio/audio.h"
#include "adc_api.h"
#include "dac.h"
#include "clock.h"
#include "oidpen_function.h"
#include "task_oidpen.h"
#include "rec_api.h"
#include "led.h"
#include "OidIndexSoundFunctionLib.h"
#include "SystemSoundPlay.h"
#include "ParsingIndexTypeLib.h"
#include "GameVariableDef.h"
#include "GameManyGroupSound.h"
#include "GameFunctionApiMGR.h"
#include "timer.h"

#define OID_DEBUG
#define OID_TAG os_time_get()*10,"OID"
#ifdef OID_DEBUG
#define oid_printf log_printf
#else
#define oid_printf(...)
#endif

extern tbool task_music_skip_check(void **priv);

WORD g_wActiveMode = OID_PLAY_MODE; //Ĭ���ڵ��ģ�?
FS_HANDLE pOidPlayFile = NULL;      // open BNF file
FS_HANDLE pSystemFile  = NULL;      //��ϵͳ�ļ�(SystemSetting.bin/OID Code Settings.XML)

WORD m_wIndexType;
u32 m_dwIndexLogic;
DWORD g_dwPreElementPhyIndex;
BYTE m_isPlayAllOpen;

WORD m_wGameType;
u32 oid_play_time_start;
u32 oid_play_time_end;
u32 oid_play_time;

//idleҪ�������ӣ�����ilde�м�ʱ										<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
WORD m_wIdleTimeOutSet;
WORD m_wIdleTimeExitGameSet;
WORD m_wIdleTimeX;
WORD m_wIdleTimeCnt;

#if OID_REC_EN
    u32 oid_rec_num;
    u32 oid_rec_play_num;
    #define OID_REC_INDEX_1     115025
    #define OID_REC_PLAY_1      115026
    #define OID_REC_INDEX_2     115029
    #define OID_REC_PLAY_2      115030
    #define OID_REC_INDEX_3     115033
    #define OID_REC_PLAY_3      115034
    #define OID_REC_INDEX_4     115023
    #define OID_REC_PLAY_4      115015
    #define OID_REC_INDEX_5     115022
    #define OID_REC_PLAY_5      115016
    #define OID_REC_INDEX_6     115021
    #define OID_REC_PLAY_6      115017

    bool oid_rec_flag = false;

    extern BYTE oidpen_play_type;
    RECORD_OP_API *oid_rec_api = NULL;
    void oidrec_mutex_init(void *priv)
    {
        oid_printf("%d\t\t%s\t\toidrec_mutex_init...\n",OID_TAG);
    }

    void oidrec_mutex_stop(void *priv)
    {
        oid_printf("%d\t\t%s\t\toidrec_mutex_stop...\n",OID_TAG);
        rec_exit(&oid_rec_api);
    }
#endif // OID_REC_EN

tbool task_oid_check_sd0(void **priv)
{
    oid_printf("%d\t\t%s\t\ttask_oid_check_sd0 !!\n",OID_TAG);

    //DEV_HANDLE *dev = *priv;
    u32 parm;
    u32 dev_status;

    tbool dev_cnt = 0;

    oid_printf("%d\t\t%s\t\toid *priv:d0x%x\n",OID_TAG, *priv);
    //check some device online
    if ((*priv != PASS_HI_TONE)||(*priv == NULL))
    {
        //  if (!dev_get_online_status(sd1, &dev_status))
        {
            //   if (dev_status != DEV_ONLINE)
            {  oid_printf("%d\t\t%s\t\ttask_oid_check_sd0 run here...\n",OID_TAG);
                *priv = dev_get_fisrt(MUSIC_DEV_TYPE, DEV_ONLINE);
            }

        }

    }

    oid_printf("%d\t\t%s\t\toid *priv:d0x%x\n",OID_TAG, *priv);
    if (*priv == sd0)
    {
        oid_printf("%d\t\t%s\t\t[PRIV SD0]\n",OID_TAG);
    }
    else if (*priv == sd1)
    {
        oid_printf("%d\t\t%s\t\t[PRIV SD1]\n",OID_TAG);
    }
    else if (*priv == usb)
    {
        oid_printf("%d\t\t%s\t\t[PRIV USB]\n",OID_TAG);
    }
    else
    {
        oid_printf("%d\t\t%s\t\t[PRIV CACHE]\n",OID_TAG);
    }

    if (*priv == NULL)
    {
        return false;
    }
    else
    {
        return true;
    }
    //check specific device online
    return true;
}



static void *task_oidpen_init(void *priv)
{
    oid_printf("%d\t\t%s\t\t\n.........%s in ........\n\n\n",OID_TAG,__func__);

    char string[10];
    extern  void itoa(u32 num,char *filename);
    u32 num = 115031;
    itoa(num,string);


    MUSIC_PLAYER *obj = NULL;

	dac_channel_on(MUSIC_CHANNEL, 0);
    set_sys_vol(sound.vol.sys_vol_l, sound.vol.sys_vol_r, FADE_ON);

    obj = oid_bnfplayer_start();

	BuildBnfFileList(obj);

    audio_set_output_buf_limit(OUTPUT_BUF_LIMIT_MUSIC_TASK);

    if (priv != PASS_HI_TONE)
    {
        //oidpen_play_tone(obj, TONE_OID_MODE);
    }
    else
    {
        m_dwOIDPhyIndex = 0xffffffff;
    }
    return obj;
}


static void task_oidpen_exit(void **priv)
{
    music_player_destroy((MUSIC_PLAYER **)priv);
    audio_set_output_buf_limit(OUTPUT_BUF_LIMIT);
    set_sys_freq(SYS_Hz);
    task_clear_all_message();
    oid_bnfplayer_stop(*priv);

#if OID_REC_EN
    rec_exit(&oid_rec_api);
    mutex_resource_release("record_play");
    //mutex_resource_release("oid_rec");
#endif
}

static u32 oidmp3_polling_to_get_oidcode(void)
{
	return get_oidcode();//����-������ͷ����
}




void task_oidpen_deal(void *hdl)
{
    oid_printf("%d\t\t%s\t\t%s in .................\n\n\n",OID_TAG,__func__);

    u8 right_flag =0;
    WORD index = 0, found_ret = 0xFFFF;
    WORD tmp_rec_time;
	WORD wRet;
    int msg = NO_MSG;
    int msg_error = MSG_NO_ERROR;
    u8 oid_start = 0;
    WORD opened_bnfnumber=0;
    pOidPlayFile = NULL;

    u8 QuestionNo = 0xFF;
    u8 last_QuestionNo = 0xFF;
    u8 first_question = 0;

	MUSIC_PLAYER *obj = (MUSIC_PLAYER *)hdl;

    while(TRUE)
    {
        msg_error = task_get_msg(0, 1, &msg);

        if (get_tone_status())  //��ʾ����δ����ǰ�����漰��������������Ϣ
        {
            oid_msg_filter(&msg);
        }
        if (task_common_msg_deal(hdl, msg) == false)   //ƽ̨��Ϣ��������
        {
            oid_printf("%d\t\t%s\t\tmsg exit, return ...............\n",OID_TAG);
            break;
        }
        #if OID_REC_EN
        rec_msg_deal_api(&oid_rec_api, msg); //record ����
        #endif
       //oid_receive_end_parse_data();//����-������ͷ����
        m_dwOIDPhyIndex = oidmp3_polling_to_get_oidcode();
        extern bool low_power_send;
        if(low_power_send)
        {
            m_dwOIDPhyIndex = 0xffffffff;
        }
        if(m_dwOIDPhyIndex < 0x10000 && m_dwOIDPhyIndex != 0xffffffff)
        {
            oid_printf("%d\t\t%s\t\t get the aio2 data is %x\n",OID_TAG,m_dwOIDPhyIndex);
            //m_dwOIDPhyIndex = read_s2_index(m_dwOIDPhyIndex, obj->fop);
            oid_printf("%d\t\t%s\t\t get the aio2 index is %x\n",OID_TAG,m_dwOIDPhyIndex);
        }


        if(m_dwOIDPhyIndex != 0xffffffff) //��������Ϊ��Ч��������ִ��
        {
            #if OID_REC_EN
            if(m_dwOIDPhyIndex == OID_REC_INDEX_1 || m_dwOIDPhyIndex == OID_REC_INDEX_2 || m_dwOIDPhyIndex == OID_REC_INDEX_3 || m_dwOIDPhyIndex == OID_REC_INDEX_4 || m_dwOIDPhyIndex == OID_REC_INDEX_5 || m_dwOIDPhyIndex == OID_REC_INDEX_6)
            {
                if(m_dwOIDPhyIndex == OID_REC_INDEX_1)
                {
                    oid_rec_num = OID_REC_PLAY_1;
                }
                else if(m_dwOIDPhyIndex == OID_REC_INDEX_2)
                {
                    oid_rec_num = OID_REC_PLAY_2;
                }
                else if(m_dwOIDPhyIndex == OID_REC_INDEX_3)
                {
                    oid_rec_num = OID_REC_PLAY_3;
                }
                else if(m_dwOIDPhyIndex == OID_REC_INDEX_4)
                {
                    oid_rec_num = OID_REC_PLAY_4;
                }
                else if(m_dwOIDPhyIndex == OID_REC_INDEX_5)
                {
                    oid_rec_num = OID_REC_PLAY_5;
                }
                else if(m_dwOIDPhyIndex == OID_REC_INDEX_6)
                {
                    oid_rec_num = OID_REC_PLAY_6;
                }

                oid_rec_flag = true;
                oid_play_time_start = os_time_get();
                oid_printf("%d\t\t%s\t\t oid rec flag is true!\n",OID_TAG);
            }
            else
            {
                oid_play_time_start = 0xFFFFFFFF;
                oid_play_time_end = 0;
                oid_printf("%d\t\t%s\t\t oid rec flag is false!\n",OID_TAG);
                oid_rec_flag = false;
            }
            if(m_dwOIDPhyIndex == OID_REC_PLAY_1 || m_dwOIDPhyIndex == OID_REC_PLAY_2 || m_dwOIDPhyIndex == OID_REC_PLAY_3 || m_dwOIDPhyIndex == OID_REC_PLAY_4 || m_dwOIDPhyIndex == OID_REC_PLAY_5 || m_dwOIDPhyIndex == OID_REC_PLAY_4 || m_dwOIDPhyIndex == OID_REC_PLAY_6)
            {
                oid_rec_play_num = m_dwOIDPhyIndex;
                if(oid_rec_play_num == 115026)
                {
                    if(is_file_exist(obj,"/JL_REC/115026.mp3") == true)
                    {
                        oidpen_play_tone(obj,TONE_REC_115026);
                    }
                    else
                    {
                        oidpen_play_tone(obj,TONE_NO_REC);
                    }
                }
                else if(oid_rec_play_num == 115030)
                {

                    if(is_file_exist(obj,"/JL_REC/115030.mp3") == true)
                    {
                        oidpen_play_tone(obj,TONE_REC_115030);
                    }
                    else
                    {
                        oidpen_play_tone(obj,TONE_NO_REC);
                    }
                }
                else if(oid_rec_play_num == 115034)
                {
                    if(is_file_exist(obj,"/JL_REC/115034.mp3") == true)
                    {
                        oidpen_play_tone(obj,TONE_REC_115034);
                    }
                    else
                    {
                        oidpen_play_tone(obj,TONE_NO_REC);
                    }
                }
                else if(oid_rec_play_num == 115015)
                {
                    if(is_file_exist(obj,"/JL_REC/115015.mp3") == true)
                    {
                        oidpen_play_tone(obj,TONE_REC_115015);
                    }
                    else
                    {
                        oidpen_play_tone(obj,TONE_NO_REC);
                    }
                }
                else if(oid_rec_play_num == 115016)
                {
                    if(is_file_exist(obj,"/JL_REC/115016.mp3") == true)
                    {
                        oidpen_play_tone(obj,TONE_REC_115016);
                    }
                    else
                    {
                        oidpen_play_tone(obj,TONE_NO_REC);
                    }
                }
                else if(oid_rec_play_num == 115017)
                {
                    if(is_file_exist(obj,"/JL_REC/115017.mp3") == true)
                    {
                        oidpen_play_tone(obj,TONE_REC_115017);
                    }
                    else
                    {
                        oidpen_play_tone(obj,TONE_NO_REC);
                    }
                }
                continue;

                //task_post_msg(NULL, 1, MSG_REC_PLAY);
            }
            #endif
            reset_poweroff_time();
            OidStopAllPlaying();
            oid_printf("%d\t\t%s\t\t get m_dwOIDPhyIndex is %d\n",OID_TAG,m_dwOIDPhyIndex);
            if( ((m_dwOIDPhyIndex >= 0x01) && (m_dwOIDPhyIndex <= 0x4A38)) \
            || ((m_dwOIDPhyIndex >= 0x5209) && (m_dwOIDPhyIndex <= 0xEFFF)) )//select BOOK
            {
                m_wIndexType = Idx_BOOKOID2;
            }
            else
            {
                if((opened_bnfnumber != file_operate_get_file_number(obj->fop)) && opened_bnfnumber)
                {   oid_printf("%d\t\t%s\t\topened_num2 = %d,file_number=%d\n",OID_TAG, opened_bnfnumber, file_operate_get_file_number(obj->fop));
                    file_operate_set_file_number(obj->fop, opened_bnfnumber);
                    wRet = file_operate_op(obj->fop, FOP_OPEN_FILE_BYNUM, NULL, NULL);
                    pOidPlayFile = (_FIL_HDL *)file_operate_get_file_hdl(obj->fop);
                    opened_bnfnumber = file_operate_get_file_number(obj->fop);
                }
                else
                {
                    oid_printf("%d\t\t%s\t\t..........opened_num2 = %d,file_number=%d................\n",OID_TAG, opened_bnfnumber, file_operate_get_file_number(obj->fop));
                }

                GetIndexType(pOidPlayFile, NULL, m_dwOIDPhyIndex,&m_wIndexType,&m_dwIndexLogic); //����������pOidPlayFile
            }
            oid_printf("%d\t\t%s\t\t...................m_wIndexType:%d,  g_wActiveMode:%d........................\n",OID_TAG, m_wIndexType, g_wActiveMode);
        }
/**************************************start switch(m_wIndexType)*****************************************************/
        switch(m_wIndexType)    //���������������Ӧ�Ĳ���?
        {
            //����
            case Idx_BOOK:
            case Idx_BOOKOID2:
            {

            }
            case Idx_CARDBOOK:
            {
				found_ret = FindFileByBnfFileList(&(obj->fop->fop_file->fs_hdl), &bnflist_hdl, m_wIndexType, m_dwOIDPhyIndex);
				if( found_ret != 0xffff)
				{
				    index = found_ret; //found a new book
                    file_operate_set_file_number(obj->fop, (index));
                    OidStopAllPlaying();
                    wRet = file_operate_op(obj->fop, FOP_OPEN_FILE_BYNUM, NULL, NULL);
                    if(!wRet)
                    {
                        oid_printf("%d\t\t%s\t\topen file OK...\n",OID_TAG);
                    }
                    else
                    {
                        oid_printf("%d\t\t%s\t\topen file err...wRet = %d\n",OID_TAG,wRet);
                    }
                    pOidPlayFile = (_FIL_HDL *)file_operate_get_file_hdl(obj->fop);
                    opened_bnfnumber = file_operate_get_file_number(obj->fop);
                    if(!pOidPlayFile)
                    {
                        break;
                    }
					wRet = CheckBookFile(pOidPlayFile);
                    oid_printf("%d\t\t%s\t\twRet : %d \n",OID_TAG, wRet);
					if(wRet == 0)
					{
						wRet = InitBookFile(pOidPlayFile);
                        oid_printf("%d\t\t%s\t\twRet : %d \n",OID_TAG, wRet);

						if(wRet == 1)
						{
							if(m_wIndexType == Idx_CARDBOOK)
							{
								GetIndexType(pOidPlayFile, pSystemFile, m_dwOIDPhyIndex,&m_wIndexType,&m_dwIndexLogic);
                                g_wActiveMode = OID_PLAY_MODE;
								break;
							}
							else
							{
								GetBookSoundGroup(pOidPlayFile);
								oid_printf("%d\t\t%s\t\tg_iOidPlayActiveSoundNo:%d\n",OID_TAG, g_iOidPlayActiveSoundNo);
								oid_printf("%d\t\t%s\t\tg_iNeedPlaySoundCnt:%d\n",OID_TAG, g_iNeedPlaySoundCnt);
								int i;
								for(i = 0; i < g_iNeedPlaySoundCnt; i++)
                                    oid_printf("%d\t\t%s\t\tg_u32IndexSoundGroup:%d\n",OID_TAG, g_u32IndexSoundGroup[i]);

								m_dwOIDPhyIndex = 0xffffffff;
							}
							g_wActiveMode = OID_PLAY_MODE;
							OidStopAllPlaying();
						}
						else
						{
							m_dwOIDPhyIndex = 0xffffffff;
						}
					}
					else
					{
                        oidpen_play_tone(obj, TONE_CANTRD);
						m_dwOIDPhyIndex = 0xffffffff;
					}
				}
				else
                {
                    oidpen_play_tone(obj, TONE_CANTRD);
                }
				m_wIndexType = 0;
				break;
			}
            case Idx_GAME:
            {
				m_wGameType = GetGameType(pOidPlayFile);
                oid_printf("%d\t\t%s\t\tm_wGameType:%d\n",OID_TAG, m_wGameType);
				switch(m_wGameType)
				{
					case GAME_MANY_GROUP:
					{
						OidStopAllPlaying();
						GetGameInitMGR(pOidPlayFile,m_dwOIDPhyIndex);
						GetGameEntrySoundMGR(pOidPlayFile);
						GetIdleTimeSetSecMGR(& m_wIdleTimeOutSet,& m_wIdleTimeExitGameSet);
						g_wActiveMode = GAME_PLAY_MODE;
                        first_question = 0;
                        last_QuestionNo = 0xFF;
                        d_uiMGRActiveQuestionNo = 0xFF;
						break;
					}
				}
				m_wIndexType = 0;
				m_dwOIDPhyIndex = 0xffffffff;
                break;
            }
            default:
                break;
        }
        /**************************************end switch(m_wIndexType)*****************************************************/
        /**************************************start switch(g_wActiveMode)*****************************************************/
        switch(g_wActiveMode)
        {
            case OID_PLAY_MODE:
            {
				switch(m_wIndexType)
				{
					case Idx_ELEMENT :
					{
						OidStopAllPlaying();
						if(GetIndexSoundGroup(pOidPlayFile, m_dwOIDPhyIndex))
						{

						}
						g_dwPreElementPhyIndex = m_dwOIDPhyIndex;
						m_wIndexType = 0;
						m_dwOIDPhyIndex = 0xffffffff;
						break;
					}
					case Idx_SCENARIO:
					{
						SetScenario(m_dwIndexLogic);
						GetScenarioSoundGroup(pOidPlayFile,m_dwIndexLogic);
						m_wIndexType = 0;
						m_dwOIDPhyIndex = 0xffffffff;
						break;
					}
					case Idx_LayerCode:
					{
						SetLayer(m_dwIndexLogic);
						GetLayerSoundGroup(pOidPlayFile,m_dwIndexLogic);
						m_wIndexType = 0;
						m_dwOIDPhyIndex = 0xffffffff;
						break;
					}
					case Idx_DIYRecord:
					{
						DWORD dwLogicDIY;
						GetLastIndexSoundGroup(pOidPlayFile);
						dwLogicDIY = m_dwIndexLogic;
						m_wIndexType = 0;
						m_dwOIDPhyIndex = 0xffffffff;

						break;
					}
					case Idx_DIYRecordPLAY:
					{
						DWORD dwLogicDIY;

						GetLastIndexSoundGroup(pOidPlayFile);

						dwLogicDIY = m_dwIndexLogic;
						m_wIndexType = 0;
						m_dwOIDPhyIndex = 0xffffffff;
						break;
					}
					case Idx_FUNC:
					{
			            //if(m_dwIndexLogic == FUNC_CHANGE_SCENERIO )
			            {
			                //�л�scenario
			                //g_wScenarioActive += 1;
			                //if(g_wScenarioActive >= g_wScenarioCnt)
			                {
			                    //
			                //    g_wScenarioActive = 0;
			                }
			                //�л�scenario,
			             //   SetScenario(g_wScenarioActive);
			                //��������
			                //������Ҫ���ţ�ֹͣ�������ţ�û������������?����������
			             //   OidStopAllPlaying();
				         //   if(GetScenarioSoundGroup(pOidPlayFile, g_wScenarioActive))
			                {
			                }
							//ȥ���˹���

			                //����Ӧ���Ƕϵ�֮ǰ�������Ŷ�
			            }
			            if(m_dwIndexLogic == FUNC_TRANSLATION )
			            {
			                //����
			                if(g_dwPreElementPhyIndex != 0xffffffff)
			                {
								//��ȡ������������
								GetElementIdxTransSoundGroup(pOidPlayFile, g_dwPreElementPhyIndex);
			                }
			            }
						if(m_dwIndexLogic == FUNC_REPEAT )
						{
							//repeat
							if(g_dwPreElementPhyIndex != 0xffffffff)
							{
								//������Ҫ���ţ�ֹͣ�������ţ�
								OidStopAllPlaying();
								if(GetIndexSoundGroup(pOidPlayFile, g_dwPreElementPhyIndex))
								{
								}
								//repeate mode en
							}
						}
						if(m_dwIndexLogic == FUNC_PLAY_ALL )
						{
							unsigned int SoundCount;    //play all

							OidStopAllPlaying();    //ֹͣ�������ţ�
							GetPlayAllSoundCount(pOidPlayFile, &g_iNeedPlaySoundCnt);	//ȡ����

							g_iOidPlayActiveSoundNo = 0;	//�ӵ�0����ʼ
							GetPlayAllSoundNoGroup(pOidPlayFile, g_iOidPlayActiveSoundNo);
							m_isPlayAllOpen = true;
						}
						m_wIndexType = 0;   //�������?
						m_dwOIDPhyIndex = 0xffffffff;	//����յ������?
						break;
					}
					case Idx_NeedSelectBook:
  					{
  					    m_wIndexType = 0;   //�������?
						m_dwOIDPhyIndex = 0xffffffff;   //����յ������?

                        oidpen_play_tone(obj, TONE_RFBOOK);
                        break;
  					}
					default:   //�������ʹ���
					    m_wIndexType = 0;   //�������?
						m_dwOIDPhyIndex = 0xffffffff;   //����յ������?
					    break;
				}
                //oid_printf("%d\t\t%s\t\tmp3_status:%d, index:%d\n",OID_TAG,MP3_GetStatus(), index );
				if(MP3_GetStatus() == MUSIC_DECODER_ST_STOP)
				{
					//��鵱ǰ���������ţ��Ա��ж������ʱ����˳�򲥷���
					//���Ƿ���������Ҫ����,
					/*������Ҫ����ʱ������ͨ������ȫ�ֱ�����֪ͨ
					g_iOidPlayActiveSoundNo >>�ӵڼ�������������Ĭ����0
					g_iNeedPlaySoundCnt		>>�ܹ��м�������Ҫ����
					>>g_u32IndexSoundGroup[]  >>����Ҫ���ŵ�������
					*/
					//oid_printf("%d\t\t%s\t\tg_iOidPlayActiveSoundNo:%d, g_iNeedPlaySoundCnt:%d\n",OID_TAG,g_iOidPlayActiveSoundNo, g_iNeedPlaySoundCnt );
					if (g_iOidPlayActiveSoundNo < g_iNeedPlaySoundCnt)
					{
						u32 dwSoundNum;
						//������Ҫ����
						//ӦҪ������������������buffer.
						//��һ���������Ź��꣬����Ҫ���ŵ�����û�в����꣬������һ����
						//��Щ���ģ�Ϊplay all,���������ſ���ʮ�����������ǻᳬ��50�ġ�
						if ((g_iOidPlayActiveSoundNo > 0) || ((g_iOidPlayActiveSoundNo == 0) && ((g_u32IndexSoundGroup[g_iOidPlayActiveSoundNo % INDEX_MULTI_SOUND_MAX] & 0x80000000) == 0)))
						{

						}
						else
						{
							g_iOidPlayActiveSoundNo++; //�����λ�?1������������һ��ֵ���������� //
						}
						/*
                            �ݵ�ǰģʽ���������ŷ��ͳ������Ա��ڸ���dlo�������о�����β�ô�����number��start��end addr
                            ȡ�������������Ŀ�ʼ������λ��
                            ׼��������һ������
                            ������������
						*/
						dwSoundNum = g_u32IndexSoundGroup[g_iOidPlayActiveSoundNo % INDEX_MULTI_SOUND_MAX];
						GetSoundInfo(pOidPlayFile, dwSoundNum, &m_dwStartAddr, &m_dwEndAddr);
						g_iOidPlayActiveSoundNo++;
						oid_printf("%d\t\t%s\t\tdwSoundNum:%d, m_dwStartAddr:0x%08X, m_dwEndAddr:0x%08X\n",OID_TAG,dwSoundNum, m_dwStartAddr, m_dwEndAddr);
                        if(m_dwStartAddr < m_dwEndAddr)
                        {
                            oidpen_play_bnffile(obj, index);
                        }
						//for palyall>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
						if(m_isPlayAllOpen == true)
						{
							if(g_iOidPlayActiveSoundNo % INDEX_MULTI_SOUND_MAX == 0)
							{
								//ȡ��һ��,
								//Ŀǰplay all���� g_iOidPlayActiveSoundNo[] buffer��С
								//����Ҫѭ��ȡֵ
								//unsigned int SoundCount;
								GetPlayAllSoundNoGroup(pOidPlayFile, g_iOidPlayActiveSoundNo);
							}
						}
						//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
					}
					else
					{
						//for palyall>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
						if(m_isPlayAllOpen == true)
						{
							m_isPlayAllOpen = false;
                        }
						//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
					}
				}

				break;
			}//case OID_PLAY_MODE:
            case OID_READAFTER_MODE:
                break;

            case GAME_PLAY_MODE:
				//����Ϸ���ͣ�������Ӧ����ϷAPI
				switch(m_wGameType)
				{
					case GAME_MANY_GROUP:
					{
						//�����Ϸ���Ƿ����������?����
						if(MP3_GetStatus() == 0/*�Ƿ��������ڲ���*/)
						{
							//��鵱ǰ���������ţ��Ա��ж������ʱ����˳�򲥷���
							//���Ƿ���������Ҫ����
							if (g_iOidPlayActiveSoundNo < g_iNeedPlaySoundCnt)
							{
								u32 dwSoundNum;
								//������Ҫ����
								if((g_u32IndexSoundGroup[g_iOidPlayActiveSoundNo % INDEX_MULTI_SOUND_MAX] & 0x8000) ==0)
								{
									//local
									//��ȡ�����Ŷ�Ӧ��������Ϣ����ʼ��ַ��size
									GetGameSoundInfoMGR(pOidPlayFile,g_u32IndexSoundGroup[g_iOidPlayActiveSoundNo % INDEX_MULTI_SOUND_MAX],&m_dwStartAddr,&m_dwEndAddr);
								}
								else
								{
									//��ʾ������Ϊgame global�е�����
									//global
									//��ȡ�����Ŷ�Ӧ��������Ϣ����ʼ��ַ��size
									GetGameGlobalSoundInfoMGR(pOidPlayFile,g_u32IndexSoundGroup[g_iOidPlayActiveSoundNo+1 % INDEX_MULTI_SOUND_MAX],&m_dwStartAddr,&m_dwEndAddr);
									g_iOidPlayActiveSoundNo++;
								}
								g_iOidPlayActiveSoundNo++;
                                if(m_dwStartAddr < m_dwEndAddr)
                                {
                                    oidpen_play_bnffile(obj, index);
                                }
							}
						}
						SetRandomValueMGR(1);
						wRet = GameMGR(pOidPlayFile,&m_wIndexType,m_dwIndexLogic,m_dwOIDPhyIndex);
						if(wRet == 0)
						{
							g_wActiveMode = OID_PLAY_MODE;
							m_wIndexType = 0;
							m_dwOIDPhyIndex = 0xffffffff;
                            first_question = 0;
                            last_QuestionNo = 0xFF;
                            d_uiMGRActiveQuestionNo = 0xFF;
                            break;
							//return;
						}
						else if(wRet == 2)
						{
							g_wActiveMode = OID_PLAY_MODE;
							m_wIndexType = 0;
							//m_dwOIDPhyIndex = 0xffffffff;
							//return;
                            first_question = 0;
                            last_QuestionNo = 0xFF;
                            d_uiMGRActiveQuestionNo = 0xFF;
							break;
						}
						else if(wRet == 1)
						{
						    if((MP3_GetStatus() == 0) && (last_QuestionNo != d_uiMGRActiveQuestionNo))
                            {
                                oid_printf("%d\t\t%s\t\t...........0x%x, 0x%x, 0x%x\n",OID_TAG,last_QuestionNo , d_uiMGRActiveQuestionNo, first_question);
                                if(first_question == 0)
                                {
                                    first_question = 1;
                                }
                                last_QuestionNo = d_uiMGRActiveQuestionNo;
                                if(d_uiMGRActiveQuestionNo == 0)
                                {

                                }
                                else if(d_uiMGRActiveQuestionNo ==1)
                                {

                                }
                                else if(d_uiMGRActiveQuestionNo ==2)
                                {

                                }
                                else if(d_uiMGRActiveQuestionNo ==3)
                                {

                                }
                            }
                            else
                            {
                                u8 i;
                                for(i =0; i < MAX_CORRECT_ANSWER; i++)
                                {
                                    if((m_dwOIDPhyIndex == d_ulMGRCorrectAnswerIndex[i]) && (m_dwOIDPhyIndex == 66400))
                                    {
                                        task_post_msg(NULL, 1, MSG_OID_ANSWER_ONE_RIGHT);
                                        break;
                                    }
                                    else if((m_dwOIDPhyIndex == d_ulMGRCorrectAnswerIndex[i]) && (m_dwOIDPhyIndex == 66401))
                                    {
                                        task_post_msg(NULL, 1, MSG_OID_ANSWER_TWO_RIGHT);
                                        break;
                                    }
                                    else if((m_dwOIDPhyIndex == d_ulMGRCorrectAnswerIndex[i]) && (m_dwOIDPhyIndex == 66402))
                                    {
                                        task_post_msg(NULL, 1, MSG_OID_ANSWER_THREE_RIGHT);
                                        break;
                                    }
                                    else if((m_dwOIDPhyIndex == d_ulMGRCorrectAnswerIndex[i]) && (m_dwOIDPhyIndex == 66403))
                                    {
                                        task_post_msg(NULL, 1, MSG_OID_ANSWER_FOUR_RIGHT);
                                        break;
                                    }
                                }
                            }
                        }
						//�緵��1��ʾ����������Ϸ������
                        #if 1
						//wIdleTimeCntΪidleʱ���ʱ���?���м�ʱ����ֻ��ildeʱ�ż�ʱ,�ж���Ҫ���?<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
						//wIdleTimeXΪ����idle�˶��ٴ�,�ж���Ҫ���?								 <<<<<<<<<<<<<<<<<<<<<<<<<<<<<
						//�ж��Ƿ������õ��˳�ʱ��
						if((m_wIdleTimeX *  m_wIdleTimeOutSet + m_wIdleTimeCnt) >=  m_wIdleTimeExitGameSet)
						{//�����˳�ʱ��
							 GetIdleTimeEixtGameSoundMGR(pOidPlayFile);
							 //������0�������ٴν������?
							 m_wIdleTimeCnt = 0;
							 //��ʱ���˳���Ϸ��
							 //game��ɺ��˳�?
							 //��mode
							 g_wActiveMode = OID_PLAY_MODE;//���?
							 //�������?
							 m_wIndexType = 0;
							 //����յ������?
							 //m_dwOIDPhyIndex = 0xffffffff; //�緵��2,���˳��ص������?�ڵ���»��?ִ�д���ֵ �Ĺ��ܣ����Բ����?
							 //return;
							 break;
						}
						else if(m_wIdleTimeCnt >=  m_wIdleTimeOutSet)
                        {
                            m_wIdleTimeX++;
                            m_wIdleTimeCnt = 0;
                            //���������ֻ��������idle��������������ֻ�����һ��?
                            if(m_wIdleTimeX < 3)
                            {
                                //������ʾ��
                                GetIdleTimeSoundMGR(pOidPlayFile,m_wIdleTimeX);//ȡ��������Ŵ�?1��ʼ
                            }
                            else
                            {
                                //��������ÿ�ζ����õ���������
                                GetIdleTimeSoundMGR(pOidPlayFile,3);
                            }
                        }
                        #endif
						m_wIndexType = 0;						//�������?
						m_dwOIDPhyIndex = 0xffffffff;			//����յ������?
					}//case GAME_MANY_GROUP:
				}
                break;
            case MP3_PLAY_MODE:
                break;
            default:
                break;
        }
/***************************************end switch(g_wActiveMode)****************************************************/

/**************************************start switch(msg)*****************************************************/
        switch (msg)
        {
            case SYS_EVENT_DEC_END:
                oid_printf("%d\t\t%s\t\t SYS_EVENT_DEC_END!\n",OID_TAG);
                music_decoder_stop(obj->dop);
                led_fre_set(C_RLED_ON_MODE);
                #if OID_REC_EN
                if(oid_rec_flag)
                {
                    oid_play_time_end = os_time_get();
                    oid_play_time = (oid_play_time_end - oid_play_time_start) / 100;
                    oid_printf("%d\t\t%s\t\t oidplaytime is %d!\n",OID_TAG,oid_play_time);
                    oid_rec_flag = false;
                    //task_post_msg(NULL, 1, MSG_REC_START_VOICE);
                    task_switch(TASK_ID_REC,NULL);
                    return;
                }
                #endif

                if(tone_var.status)
                {
                    tone_var.status = 0;
                    tone_var.idx    = 0;
                }
                break;
            case SYS_EVENT_PLAY_SEL_END:
                oid_printf("%d\t\t%s\t\t SYS_EVENT_PLAY_SEL_END!\n",OID_TAG);
                music_decoder_stop(obj->dop);
                led_fre_set(C_RLED_ON_MODE);
                if(tone_var.idx == TONE_WARNING)
                {
                    task_post_msg(NULL, 1, MSG_LOW_POWER);
                }
                 tone_var.status = 0;
                 tone_var.idx    = 0;
                 break;
            case MSG_VOL_UP:
                //if(sound.vol.sys_vol_l == get_max_sys_vol(0))
                if(sound.vol.sys_vol_l == USER_VOL_5)
                {
                    oidpen_play_tone(obj, TONE_VOL_5);
                }
                else if(sound.vol.sys_vol_l == USER_VOL_4)
                {
                    oidpen_play_tone(obj, TONE_VOL_4);
                }
                else if(sound.vol.sys_vol_l == USER_VOL_3)
                {
                    oidpen_play_tone(obj, TONE_VOL_3);
                }
                else if(sound.vol.sys_vol_l == USER_VOL_2)
                {
                    oidpen_play_tone(obj, TONE_VOL_2);
                }
                else if(sound.vol.sys_vol_l == USER_VOL_1)
                {
                    oidpen_play_tone(obj, TONE_VOL_1);
                }

                break;
            case MSG_VOL_DOWN:
                if(sound.vol.sys_vol_l == USER_VOL_5)
                {
                    oidpen_play_tone(obj, TONE_VOL_5);
                }
                else if(sound.vol.sys_vol_l == USER_VOL_4)
                {
                    oidpen_play_tone(obj, TONE_VOL_4);
                }
                else if(sound.vol.sys_vol_l == USER_VOL_3)
                {
                    oidpen_play_tone(obj, TONE_VOL_3);
                }
                else if(sound.vol.sys_vol_l == USER_VOL_2)
                {
                    oidpen_play_tone(obj, TONE_VOL_2);
                }
                else if(sound.vol.sys_vol_l == USER_VOL_1)
                {
                    oidpen_play_tone(obj, TONE_VOL_1);
                }
                break;
            case MSG_POWER_OFF:
                {       //oid_printf("%d\t\t%s\t\tMSG_POWER_OFFSYS_EVENT_PLAY_TONE_END������������������������������\n",OID_TAG);

                }
                break;
            case MSG_SD0_OFFLINE:
                fs_close(&bnflist_hdl);
                break;
            case MSG_SD0_ONLINE:
             //   BuildBnfFileList(obj);
                break;

            #if OID_REC_EN

            case MSG_REC_INIT:
                oid_printf("%d\t\t%s\t\tMSG_REC_INIT\n",OID_TAG);
                break;

            case MSG_REC_START:
                oid_printf("%d\t\t%s\t\t MSG_REC_START\n",OID_TAG);
                oidpen_play_type=0;
                dac_set_samplerate(48000, 0);
                break;
            case MSG_HALF_SECOND:
                    tmp_rec_time = rec_get_enc_time(oid_rec_api);
                    if (tmp_rec_time) {
                        oid_printf("%d\t\t%s\t\t rec time %d:%d\n",OID_TAG, tmp_rec_time / 60, tmp_rec_time % 60);
                    }
                break;
            #endif          // OID_REC_EN
            default:
                break;
        }
/**************************************end switch(msg)*****************************************************/
    }//end while(TRUE)

    fs_close(&bnflist_hdl);
}



const TASK_APP task_oidpen_info = {
    .skip_check = task_oid_check_sd0,
    .init 		= task_oidpen_init,
    .exit 		= task_oidpen_exit,
    .task 		= task_oidpen_deal,
    .key 		= &task_oidpen_key,
};

