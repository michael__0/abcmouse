#include "common/common.h"
#include "fs_io.h"
#include "file_op_err.h"
#include "msg.h"
#include "task_common.h"
#include "warning_tone.h"
#include "ff_api.h"
#include "OidIndexSoundFunctionLib.h"
#include "ParsingIndexTypeLib.h"
#include "music_player.h"
#include "oidpen_function.h"
#include "task_manager.h"
#include "two_wire.h"
#include "task_oidpen.h"
#include "timer.h"


#define OIDPEN_TAG os_time_get()*10,"OIDPEN"
#define OIDPEN_DEBUG
#ifdef OIDPEN_DEBUG
#define oidpen_printf log_printf
#else
#define oidpen_printf(...)
#endif


u32 m_dwOIDPhyIndex = 0xffffffff;
u32 m_dwStartAddr;
u32 m_dwEndAddr;

const char bnflist_name[]="/bnflist.bin";
_FIL_HDL bnflist_hdl;

BYTE oidpen_play_type = 0;//0:first start 1:bnf, 2:tone

typedef struct CardBookRangetag{
    u32 RangeStart;            		//range start
	u32 RangeEnd;					//range end
} CARDBOOKRANGE;

//记录书本文件进入的信息，以便要切换书本时查找书本对应的是那个文件
typedef struct bookEntrytag{
    u16 Index;                          //BOOK INDEX
    u32 OID3BookIndex;            	    //OID3 BOOK ENTYR CODE
	u32 OID2BookIndex;				    //OID2 BOOK ENTYR CODE
	u32 CardBookRangeCnt;			    //CARD BOOK RANGE ENTYR RANGE COUNT
	CARDBOOKRANGE CardBookRange[3];     //CARD BOOK RANGE
} BOOKENTRYINFO;

static volatile u32 poweroff_time = AUTO_POWEROFF_TIME;

void reset_poweroff_time(void)
{
    poweroff_time = AUTO_POWEROFF_TIME;
}

void auto_poweroff_detect(void)
{
    MUSIC_PLAYER* obj;

    if(poweroff_time == 0)
        return;
    obj = get_music_player_handle();

    poweroff_time--;
    //oidpen_printf("%d\t\t%s\t\toff_time:%d\n",OIDPEN_TAG, poweroff_time);
    //if(obj->fop==NULL)
    //    oidpen_printf("%d\t\t%s\t\tobj->fop==NULL...\n",OIDPEN_TAG);
    //if(obj->dop==NULL)
    //    oidpen_printf("%d\t\t%s\t\tobj->dop==NULL...\n",OIDPEN_TAG);
    if(poweroff_time == 120 && obj != NULL)
    {
        //oidpen_printf("%d\t\t%s\t\tmusic_player_get_status(obj):%d\n",OIDPEN_TAG, music_player_get_status(obj));
        if((music_player_get_status(obj)==MUSIC_DECODER_ST_PLAY)&& (task_get_cur() == TASK_ID_MUSIC))
        {
            reset_poweroff_time();
        }
        else if(task_get_cur() == TASK_ID_OID)
        {
            oidpen_play_tone(obj, TONE_AREYOU_HERE);
            //tone_play(TONE_AREYOU_HERE, 0);
        }
        else
        {
            tone_play(TONE_AREYOU_HERE, 0);
        }
    }
    else if(poweroff_time==0)
    {
        task_post_msg(NULL, 1, MSG_POWER_OFF_AUTO);
    }
}




LOOP_DETECT_REGISTER(oid_poweroff_detect) = {
    .time = 500,
    .fun  = auto_poweroff_detect,
};

//播放的音频起始地址，主要是bnf解码时使用
u32 Get_Bnf_Sound_StartAddr(void)
{
    return m_dwStartAddr;
}

u32 Get_Bnf_Sound_EndAddr(void)
{
    return m_dwEndAddr;
}

//打印BNF文件list
void printBnfFileList(_FIL_HDL *f_hdl)
{
    oidpen_printf("%d\t\t%s\t\t\n\n\n******************************BookFileList******************************************\n",OIDPEN_TAG);
    u16 i;
    BOOKENTRYINFO m_BnfInfo;

    fs_seek(f_hdl, 0, 0);
    while(TRUE)
    {
        memset(&m_BnfInfo, 0x00, sizeof(m_BnfInfo));

        fs_read(f_hdl, (void *)&m_BnfInfo, sizeof(m_BnfInfo));

        if(m_BnfInfo.Index == 0)
            break;

        oidpen_printf("%d\t\t%s\t\t i:%d, OID2:0x%08X,  OID3:0x%08X, cnt:%d",OIDPEN_TAG, m_BnfInfo.Index, m_BnfInfo.OID2BookIndex, m_BnfInfo.OID3BookIndex,m_BnfInfo.CardBookRangeCnt);
        for(i = 0; i < m_BnfInfo.CardBookRangeCnt; i++)
        {
           oidpen_printf("[0x%08X, 0x%08X]", m_BnfInfo.CardBookRange[i].RangeStart, m_BnfInfo.CardBookRange[i].RangeEnd);
        }
        oidpen_printf("\n");
    }
    oidpen_printf("%d\t\t%s\t\t*******************************************************************************\n\n\n",OIDPEN_TAG);
}

bool checkpath_ext(char *fname, char const *ext)
{
    char *str = fname;
    u8   fname_len = 0;

    if (!ext) { //不匹配
        return false;
    }

    while (*str++ != '\0') {
        fname_len ++;
    }

   // printf("fext_name = %s\n",OIDPEN_TAG, fname + fname_len - 3);
    if (!memcmp(fname + fname_len - 3, ext, 3)) {
        return true;
    }

    return false;
}

bool createbnflist(void)
{
    oidpen_printf("%d\t\t%s\t\tBuildBnfFileList in ....\n",OIDPEN_TAG);
    bool check = FALSE;
    bool ret;
	u32 dwOID3BookCode = 0;
	u32 dwOID2BookCode=0;
	WORD wRangeCnt = 0;
	WORD wPos;
	WORD wRet;
	u32 dwRangeStart,dwRangeEnd;

	WORD wFileCnt;
	WORD i = 0;
	u32 wVer =0;

    BOOKENTRYINFO m_BnfInfo;
    WORD pCardBookCntBuf[1];
    CARDBOOKRANGE pCardBookBuf[3];
    FILE_OPERATE *fop_api =NULL;
    fop_api = file_operate_creat();
    if (fop_api == NULL)
    {
        oidpen_printf("%d\t\t%s:\tcreat fop_api err...\n",OIDPEN_TAG);
        return false;
    }
    file_operate_set_dev(fop_api, (u32)sd0);
    ret = file_api_creat(fop_api->fop_file, (void *)fop_api->fop_info->dev, 0);
    if (ret == false)
    {
        oidpen_printf("%d\t\t%s:\tcreat file_api err...\n",OIDPEN_TAG);
        file_operate_destroy(&fop_api);
        return false;
    }
    _FIL_HDL *f_p = file_operate_get_file_hdl(fop_api);
    _FS_HDL  *fs_p = file_operate_get_fs_hdl(fop_api);
    if(&(fop_api->fop_file->fs_hdl) == NULL)
    {
        oidpen_printf("%d\t\t%s:\tfs_hdl err...\n",OIDPEN_TAG);
        return false;
    }
    memset(&bnflist_hdl, 0x00, sizeof(bnflist_hdl));
    wRet = fs_get_file_bypath(fs_p, &bnflist_hdl, (void *)bnflist_name);

    if (wRet == FILE_OP_NO_ERR)
    {
        oidpen_printf("%d\t\t%s\t\t-------find bnflist.bin--------\n",OIDPEN_TAG);
        return false;
    }
    else
    {
        oidpen_printf("%d\t\t%s\t\t-------creat bnflist.bin start--------\n",OIDPEN_TAG);
        wRet = fs_open(fs_p, &bnflist_hdl, (void *)bnflist_name, FA_CREATE_NEW|FA_WRITE);
        if(wRet == 0)
        {
             oidpen_printf("%d\t\t%s\t\t-------creat bnflist.bin success!..........................\n",OIDPEN_TAG);
        }
        else
        {
            oidpen_printf("%d\t\t%s\t\t-------creat bnflist.bin fail!, wRet=%d..........................\n",OIDPEN_TAG,wRet);
            return false;
        }
    }

    file_operate_set_file_sel_mode(fop_api, PLAY_SPEC_FILE);
	wFileCnt = file_operate_get_file_total(fop_api);                       //取得MP3BNF文件总数
    oidpen_printf("%d\t\t%s\t\t wFileCnt:%d\n",OIDPEN_TAG,  wFileCnt);

	for(i = 1;i < (wFileCnt+1); i++)                                        //逐个扫描所有文件，建立列表, 文件序号从1开始。
	{
        memset(&m_BnfInfo, 0x00, sizeof(m_BnfInfo));
		file_operate_set_file_number(fop_api, i);
		oidpen_printf("%d\t\t%s\t\t open i:%d\n",OIDPEN_TAG,  i);
		file_operate_op(fop_api, FOP_OPEN_FILE_BYNUM, NULL, NULL); //按序号打开文件

        void* f_path=NULL;
        file_operate_get_file_name(fop_api,&f_path);
        printf("%d file_name :%s \n ",i, f_path);
        check = checkpath_ext(f_path, "BNF");
        oidpen_printf("%d\t\t%s\t\tcheckpath_ext():%d\n",OIDPEN_TAG,check);

        if(!check)
        {
            fs_close(f_p);
            continue;
        }
		//memcpy(m_BookFileList[i-1].pcFileName,f_path,strlen(f_path));//保存对应的文件名

		//初始range总数
		wRangeCnt = 0;

		//读版本号
        fs_seek(f_p, 0, FS_FILE_BEGIN);
		fs_read(f_p, (u8*)&wVer, 2);

		if(wVer >= 8)
		{
			//读书码
			fs_seek(f_p, 0, 8);
			fs_read(f_p, (u8*)&dwOID2BookCode,4);

			fs_seek(f_p, 0 ,12);
			fs_read(f_p, (u8*)&dwOID3BookCode, 4);

			//读范围
			fs_seek(f_p, 0 ,212);
			fs_read(f_p,(u8*)&pCardBookCntBuf,2);

			fs_seek(f_p , 0,212+2);
			fs_read(f_p, (u8*)&pCardBookBuf,pCardBookCntBuf[0]*8);
		}
		else
		{
            #if 0
			//取得文件中直接进入书本文件的range
			//检查文件合法性，并初始化
			//检查书本
			wRet = CheckBookFile(f_p);
			if(wRet == 0)
			{
				wRet = InitBookFile(f_p);	//检查通过，初始化
				if(wRet == 1)
				{
					//初始化成功
				}
				else
				{
				    oidpen_printf("%d\t\t%s\t\tInitBookFile fail, wRet : %d \n",OIDPEN_TAG, wRet);
				    fs_close(f_p);
					continue;
				}
			}
			else
			{
                oidpen_printf("%d\t\t%s\t\tCheckBookFile fail, wRet : %d \n",OIDPEN_TAG, wRet);
			    fs_close(f_p);
				continue;
			}
            #endif
			//取得文件中记录的书码，分OID2,OID3两种
			GetBookPhyIndex(f_p, &dwOID3BookCode, &dwOID2BookCode);

			//取得书本的范围，此范围是在类型是Idx_CARDBOOK时，于此确定码值在那个文件中的范围中
			//以确定打开哪个文件
			wPos = GetCardBookFirstIndexRange(f_p,&dwRangeStart,&dwRangeEnd);
			while(wPos)
			{
				//保存一组范围, dwRangeStart,pdwRangeEnd
				 oidpen_printf("%d\t\t%s\t\tdwRangeStart:%x , dwRangeEnd:%x\n ",dwRangeStart,dwRangeEnd);
				pCardBookBuf[wRangeCnt].RangeStart = dwRangeStart;
				pCardBookBuf[wRangeCnt].RangeEnd = dwRangeEnd;
				wRangeCnt++;

				//取得下一组
				wPos =  GetCardBookNextIndexRange(f_p,wPos,&dwRangeStart, &dwRangeEnd);
			}
			//保存range总数
			pCardBookCntBuf[0] = wRangeCnt;
		}

        //保存到结构体
        m_BnfInfo.Index = i;
        m_BnfInfo.OID2BookIndex = dwOID2BookCode;
        m_BnfInfo.OID3BookIndex = dwOID3BookCode;

        //保存range总数
        wRangeCnt = pCardBookCntBuf[0];
        m_BnfInfo.CardBookRangeCnt = wRangeCnt;
        memcpy(m_BnfInfo.CardBookRange,pCardBookBuf,wRangeCnt*8);

        //数据写入列表文件
        fs_write(&bnflist_hdl, (void *)&m_BnfInfo, sizeof(m_BnfInfo));
        fs_sync(&bnflist_hdl);

		//下一个BNF文件
        fs_close(f_p);
	}

    printBnfFileList(&bnflist_hdl);

    file_operate_destroy(&fop_api);
    return true;
}
/* 扫描文件夹下所有bnf文件，并将文件的信息读出建立书本列表，m_BookFileList[]
以便在文件个数较多时，加快选书时间 */
void BuildBnfFileList(MUSIC_PLAYER* obj)
{
    oidpen_printf("%d\t\t%s\t\tBuildBnfFileList in ....\n",OIDPEN_TAG);
    bool check = FALSE;

	u32 dwOID3BookCode = 0;
	u32 dwOID2BookCode=0;
	WORD wRangeCnt = 0;
	WORD wPos;
	WORD wRet;
	u32 dwRangeStart,dwRangeEnd;

	WORD wFileCnt;
	WORD i = 0;
	u32 wVer =0;

    BOOKENTRYINFO m_BnfInfo;
    WORD pCardBookCntBuf[1];
    CARDBOOKRANGE pCardBookBuf[3];

    _FIL_HDL *f_p = file_operate_get_file_hdl(obj->fop);
    _FS_HDL  *fs_p = file_operate_get_fs_hdl(obj->fop);

    memset(&bnflist_hdl, 0x00, sizeof(bnflist_hdl));
    wRet = fs_get_file_bypath(fs_p, &bnflist_hdl, (void *)bnflist_name);

    if (wRet == FILE_OP_NO_ERR)
    {
        oidpen_printf("%d\t\t%s\t\t-------find bnflist.bin--------\n",OIDPEN_TAG);

        return;
    }
    else
    {

        oidpen_printf("%d\t\t%s\t\t-------creat bnflist.bin start--------\n",OIDPEN_TAG);

        wRet = fs_open(fs_p, &bnflist_hdl, (void *)bnflist_name, FA_CREATE_NEW|FA_WRITE);

        if(wRet == 0)
        {
             oidpen_printf("%d\t\t%s\t\t-------creat bnflist.bin success!..........................\n",OIDPEN_TAG);
        }
        else
        {
            oidpen_printf("%d\t\t%s\t\t-------creat bnflist.bin fail!, wRet=%d..........................\n",OIDPEN_TAG,wRet);
            return;
        }
    }

    file_operate_set_file_sel_mode(obj->fop, PLAY_SPEC_FILE);
	wFileCnt = file_operate_get_file_total(obj->fop);                       //取得MP3BNF文件总数
    oidpen_printf("%d\t\t%s\t\t wFileCnt:%d\n",OIDPEN_TAG,  wFileCnt);

	for(i = 1;i < (wFileCnt+1); i++)                                        //逐个扫描所有文件，建立列表, 文件序号从1开始。
	{
        memset(&m_BnfInfo, 0x00, sizeof(m_BnfInfo));
		file_operate_set_file_number(obj->fop, i);
		oidpen_printf("%d\t\t%s\t\t open i:%d\n",OIDPEN_TAG,  i);
		file_operate_op(obj->fop, FOP_OPEN_FILE_BYNUM, NULL, NULL); //按序号打开文件

        void* f_path=NULL;
        file_operate_get_file_name(obj->fop,&f_path);
        oidpen_printf("%d\t\t%s\t\t %d file_name :%s \n ",OIDPEN_TAG, i, f_path);
        check = checkpath_ext(f_path, "BNF");
        oidpen_printf("%d\t\t%s\t\tcheckpath_ext():%d\n",OIDPEN_TAG,check);

        if(!check)
        {
            fs_close(f_p);
            continue;
        }
		//memcpy(m_BookFileList[i-1].pcFileName,f_path,strlen(f_path));//保存对应的文件名

		//初始range总数
		wRangeCnt = 0;

		//读版本号
        fs_seek(f_p, 0, FS_FILE_BEGIN);
		fs_read(f_p, (u8*)&wVer, 2);

		if(wVer >= 8)
		{
			//读书码
			fs_seek(f_p, 0, 8);
			fs_read(f_p, (u8*)&dwOID2BookCode,4);

			fs_seek(f_p, 0 ,12);
			fs_read(f_p, (u8*)&dwOID3BookCode, 4);

			//读范围
			fs_seek(f_p, 0 ,212);
			fs_read(f_p,(u8*)&pCardBookCntBuf,2);

			fs_seek(f_p , 0,212+2);
			fs_read(f_p, (u8*)&pCardBookBuf,pCardBookCntBuf[0]*8);
		}
		else
		{
            #if 0
			//取得文件中直接进入书本文件的range
			//检查文件合法性，并初始化
			//检查书本
			wRet = CheckBookFile(f_p);
			if(wRet == 0)
			{
				wRet = InitBookFile(f_p);	//检查通过，初始化
				if(wRet == 1)
				{
					//初始化成功
				}
				else
				{
				    oidpen_printf("%d\t\t%s\t\tInitBookFile fail, wRet : %d \n",OIDPEN_TAG, wRet);
				    fs_close(f_p);
					continue;
				}
			}
			else
			{
                oidpen_printf("%d\t\t%s\t\tCheckBookFile fail, wRet : %d \n",OIDPEN_TAG, wRet);
			    fs_close(f_p);
				continue;
			}
            #endif
			//取得文件中记录的书码，分OID2,OID3两种
			GetBookPhyIndex(f_p, &dwOID3BookCode, &dwOID2BookCode);

			//取得书本的范围，此范围是在类型是Idx_CARDBOOK时，于此确定码值在那个文件中的范围中
			//以确定打开哪个文件
			wPos = GetCardBookFirstIndexRange(f_p,&dwRangeStart,&dwRangeEnd);
			while(wPos)
			{
				//保存一组范围, dwRangeStart,pdwRangeEnd
				 oidpen_printf("%d\t\t%s\t\tdwRangeStart:%x , dwRangeEnd:%x\n ",OIDPEN_TAG, dwRangeStart,dwRangeEnd);
				pCardBookBuf[wRangeCnt].RangeStart = dwRangeStart;
				pCardBookBuf[wRangeCnt].RangeEnd = dwRangeEnd;
				wRangeCnt++;

				//取得下一组
				wPos =  GetCardBookNextIndexRange(f_p,wPos,&dwRangeStart, &dwRangeEnd);
			}

			//保存range总数
			pCardBookCntBuf[0] = wRangeCnt;

		}

        //保存到结构体
        m_BnfInfo.Index = i;
        m_BnfInfo.OID2BookIndex = dwOID2BookCode;
        m_BnfInfo.OID3BookIndex = dwOID3BookCode;

        //保存range总数
        wRangeCnt = pCardBookCntBuf[0];
        m_BnfInfo.CardBookRangeCnt = wRangeCnt;
        memcpy(m_BnfInfo.CardBookRange,pCardBookBuf,wRangeCnt*8);

        //数据写入列表文件
        fs_write(&bnflist_hdl, (void *)&m_BnfInfo, sizeof(m_BnfInfo));
        fs_sync(&bnflist_hdl);

		//下一个BNF文件
        fs_close(f_p);
	}

    printBnfFileList(&bnflist_hdl);
 }



/* 返回是列表内的第几本书，找不到返回0xffff */
/*每次读取一个数据包大小来根据类型判断*/
WORD FindFileByBnfFileList(_FS_HDL *fs_hdl, _FIL_HDL *f_hdl, WORD wIndexType,u32 dwOidPhyIndex)
{
    u16 i, findIndex = 0xFFFF;
    BOOKENTRYINFO m_BnfInfo;

    fs_seek(f_hdl, 0, 0);

    while(TRUE)
    {
        memset(&m_BnfInfo, 0x00, sizeof(m_BnfInfo));
        fs_read(f_hdl, (void *)&m_BnfInfo, sizeof(m_BnfInfo));

        if(m_BnfInfo.Index == 0)
            break;

        if(Idx_CARDBOOK == wIndexType)//Idx_CARDBOOK看是否在范围内
        {
            for(i = 0; i < m_BnfInfo.CardBookRangeCnt; i++)
            {


                if(dwOidPhyIndex >= m_BnfInfo.CardBookRange[i].RangeStart
                && (dwOidPhyIndex <= m_BnfInfo.CardBookRange[i].RangeEnd)
                )
                findIndex = m_BnfInfo.Index;
            }
        }
        else //Idx_BOOK, Idx_BOOKOID2看是码值是否相等
        {
            if((dwOidPhyIndex == m_BnfInfo.OID2BookIndex)\
                ||(dwOidPhyIndex == m_BnfInfo.OID3BookIndex))
            {
                findIndex = m_BnfInfo.Index;
            }

        }
    }

    oidpen_printf("%d\t\t%s\t\t FindFileByBnfFileList in, findIndex = 0x%04X\n",OIDPEN_TAG, findIndex);
	return findIndex;
}

extern u8 ble_seed_data[8];
//根据数据包获取点读码值
void parse_receive_data(u8 *data)
{
    m_dwOIDPhyIndex = ((u32)data[4]<<24)|((u32)data[5]<<16)|((u32)data[6]<<8)|((u32)data[7]);
    memcpy(ble_seed_data, data, 8);
    oidpen_printf("%d\t\t%s\t\tm_dwOIDPhyIndex:0x%08x\n",OIDPEN_TAG, m_dwOIDPhyIndex);
    reset_poweroff_time();
}

u32 oid_receive_end_parse_data()
{
    u8 data[8]={0};
    extern bool master_readfrom_slave(u8 *data);
    if(master_readfrom_slave(data))
    {
        parse_receive_data(data);
    }


    return m_dwOIDPhyIndex;
}


static void bnfplayer_mutex_init(void *priv)
{
    oidpen_printf("%d\t\t%s\t\tbnfplayer_mutex_init in.\n",OIDPEN_TAG);

}

static void bnfplayer_mutex_stop(void *priv)
{
   // oidpen_printf("%d\t\t%s\t\tbnfplayer_mutex_stop:0x%x\n",OIDPEN_TAG, priv);
    MUSIC_PLAYER *obj = priv;

    if (obj ) {
        music_player_destroy(&obj);
    }
}


MUSIC_PLAYER *oid_bnfplayer_start(void)
{
    MUSIC_PLAYER *obj = NULL;
    obj = bnf_player_creat();
    oidpen_play_type=0;
    if (obj) {
       mutex_resource_apply("bnfplayer", 3, bnfplayer_mutex_init, bnfplayer_mutex_stop, obj);
    }
    return obj;
}


tbool oidpen_play_bnffile(MUSIC_PLAYER *obj, u32 filenum)
{
    if (obj == NULL) {
        return false;
    }

    OidStopAllPlaying(); //不加可能会死机
    if(oidpen_play_type == 2 || oidpen_play_type==0)//从tone切过来
    {
       tone_var.status = 0;//切断提示音播放
       set_bnfplayer_parame(obj);
    }

    file_operate_set_file_number(obj->fop, filenum);
    oidpen_play_type = 1;//播放bnf文件

    return music_player_play(obj, NULL, 0);
}

MUSIC_PLAYER * oidpen_play_tone(MUSIC_PLAYER *obj, u8 index)
{
    #if oid_rec_en
    oid_rec_flag = false;
    #endif

    void* tone_name = NULL;
    oidpen_printf("%d\t\t%s\t\t oidpen_play_tone:index = %d\n",OIDPEN_TAG,index);

    if (get_going_to_pwr_off()||obj==NULL)
    { //关机过程不播其它提示音
        return obj;
    }


    tone_name = get_tone_name(index);

    if((tone_var.status == 1)&&(tone_var.idx == index)) //正在播放提示音,切换一个相同的提示音就不要播了。
    {
        return obj;
    }


    if(tone_name)
    {
        oidpen_printf("%d\t\t%s\t\t oidpen tone play uri is %s\n",OIDPEN_TAG,tone_name);
        tone_var.idx = index;
        tone_var.rpt_mode = 0;
        tone_var.status = 1;

        OidStopAllPlaying();//不加暴力测试可能会死机

        if(oidpen_play_type==1 ||oidpen_play_type==0)//切换播放类型再重新设置
            set_tone_parame(obj);

        file_operate_set_path(obj->fop, tone_name, 0);
        oidpen_play_type=2;//播放提示音

        music_player_play(obj, NULL, 0);
    }

    return obj;
}


void oid_bnfplayer_stop(void *priv)
{
    mutex_resource_release("bnfplayer");
    task_common_msg_deal(priv, NO_MSG);
}


//过滤与解码器操作相关的消息
void oid_msg_filter(int *msg)
{
    /*
    switch (*msg) {
    case MSG_MUSIC_PP:
    case MSG_MUSIC_NEXT_FILE:
    case MSG_MUSIC_PREV_FILE:
    case MSG_MUSIC_FF:
    case MSG_MUSIC_FR:
    case MSG_MUSIC_AB_RPT:
    case MSG_HALF_SECOND:
        *msg = NO_MSG;
        break;
    }
    */
    if(get_going_to_pwr_off())
    {
        if(*msg == MSG_POWER_OFF_HOLD||*msg == MSG_POWER_KEY_UP || *msg == SYS_EVENT_DEC_END || *msg == MSG_MUSIC_PP)
        {

        }
        else
        {
            *msg = NO_MSG;
        }
    }
    extern bool low_power_send;
    if(low_power_send)
    {
        if(*msg == MSG_VOL_DOWN || *msg == MSG_VOL_UP)
        {
            *msg = NO_MSG;
        }
    }
}


tbool check_turnto_oid_mode()//检测是否跳转到OID
{
    u32 param;
        extern u32 oid_receive_end_parse_data();
        extern u32 m_dwOIDPhyIndex ;
        if(oid_receive_end_parse_data() != 0xffffffff)
        {
           #if 1//测试广播
                task_post_msg(NULL, 1, MSG_BT_OID_DATA);
                m_dwOIDPhyIndex = 0xffffffff;
            #else//检测跳转
            if (dev_get_online_status(sd0, (void *)&param) == DEV_ERR_NONE) {
                if (param == DEV_ONLINE) {
                    oidpen_printf("%d\t\t%s\t\tTURN to----OIDPEN\n",OIDPEN_TAG);
                    task_switch(TASK_ID_OID, PASS_HI_TONE);//跳过模式提示音
                    return TRUE;
                }
                else
                {
                    m_dwOIDPhyIndex = 0xffffffff;
                }

            }
            #endif
        }

    return FALSE;
}




bool is_file_exist(MUSIC_PLAYER* obj, char *url)
{
    oidpen_printf("%d\t\t%s\t\t is file exist function in ....\n",OIDPEN_TAG);
    bool check = FALSE;
    s32 wRet;

    _FIL_HDL *f_p = file_operate_get_file_hdl(obj->fop);
    _FS_HDL  *fs_p = file_operate_get_fs_hdl(obj->fop);

    //memset(&bnflist_hdl, 0x00, sizeof(bnflist_hdl));

    wRet = fs_get_file_bypath(fs_p, f_p, (void *)url);

    if (wRet == FILE_OP_NO_ERR)
    {
        oidpen_printf("%d\t\t%s\t\t-------find file:%s  suc--------\n",OIDPEN_TAG,url);

        return true;
    }
    wRet = fs_open(fs_p, f_p, (void *)url, FA_WRITE);

    if(wRet == 0)
    {
        fs_close(f_p);
        oidpen_printf("%d\t\t%s\t\t-------find file:%s suc--------\n",OIDPEN_TAG,url);
        return true;
    }
    else
    {
        fs_close(f_p);
        oidpen_printf("%d\t\t%s\t\t error:-------find file:%s error--------,ret is %d\n",OIDPEN_TAG,url,wRet);
        return false;
    }   
 }




