/*-----------------------------------------------------------------------------------------------------------------------------------------
FileDiscribe
FileName	:
Author		:Qin
Data		:
Email		:
Description	:
Version		:
Hardware&IDE:
Copyright(C),SONIX TECHNOLOGY Co.,Ltd.
History		:
//---------------------------------------------------------------------------------------------------------------------------------------*/



//#include "mdi_audio.h"

#include "OidIndexSoundFunctionLib.h"
#include "SystemSoundPlay.h"
//#include "SystemInfoOffsetDef.h"
#include "ParsingIndexTypeLib.h"
#include "GameVariableDef.h"


#include "music_decoder.h"
#include "fs_io.h"




//-----------------------------------------------------------------------------------------------------------------------------------------
//Function:
//  OidStopAllPlaying
//Description:
//  ֹͣ���е�ǰ�������Ŷ�����δ��ɲ��ŵĶ���
//Input:
//
//OutPut:
//
//Return:
//
//Others:
// Ҫ��ֹͣ�������ţ���Ϸ���̽ӿ��е��ô�ֹͣ�������ţ��Ա����һ�²������������
//-----------------------------------------------------------------------------------------------------------------------------------------
void OidStopAllPlaying(void)
{
    //Stop Recording and Playback
    //Ҫ����ƽ̨��ֹͣ�������ź���
	music_decoder_stop_two();
}


//-----------------------------------------------------------------------------------------------------------------------------------------
//Function:
//  MP3_GetStatus
//Description:
//  ���mp3�Ƿ����ڲ���
//Input:
//
//OutPut:
//
//Return:
//  0 :����������
//	1 :�������ڲ���
//Others:
// Ҫ׼ȷ���ص�ǰ���ŵ�״̬����Ϸ���̽ӿ��е��ô��ж��Ƿ����������ţ��Ծ����Ƿ����һ�²������������
//-----------------------------------------------------------------------------------------------------------------------------------------
WORD MP3_GetStatus(void)
{
	WORD ret = 1;

    //Ҫ����ƽ̨�ĵ�ǰ����״̬
	//if((ret = mdi_audio_is_playing(MDI_AUDIO_PLAY_ANY)) == 0)
	if(music_decoder_get_status_two() != MUSIC_DECODER_ST_PLAY)
	{
		ret = MUSIC_DECODER_ST_STOP;
	}

	return ret;
}

//��ȡ��ǰ�ļ�ָ��λ��
//  FS_HANDLE pFile 		/* [IN] File object */
//  u32 *dwPreFilePos,   	/* [IN] �����ļ�ָ���� */
//����ֵ 1�ɹ���0ʧ��
WORD FS_GetFilePosition(FS_HANDLE pFile,u32 *dwPreFilePos)
{
	WORD ret = 1;

	*dwPreFilePos = fs_tell(pFile);

	return ret;
}

//�ƶ��ļ�ָ��
//  FS_HANDLE pFile 	/* [IN] File object */
//  u32 dwSeekAddr,   /* [IN] File read/write pointer */
//  u32 dwSeekWay	SEEK_SET, SEEK_CUR, SEEK_END/* [IN] ��ͷ/��ǰ/��βseek����Ҫ��λ�ã� */
//����ֵ 1�ɹ���0ʧ��
WORD FS_Seek(FS_HANDLE pFile,u32 dwSeekAddr,u32 dwSeekWay)
{
	DWORD dwRet=0;

    dwRet = fs_seek(pFile,dwSeekWay,dwSeekAddr);

	if(g_wLibDebugOutputCtrl == ERR_LIB_DEBUG_INIT_BOK_FILE)
	{
		printf("fs_seek() pFile=%d,sekRet%d,addr=0x%x\r\n",(DWORD)pFile,dwRet,dwSeekAddr);
	}
	//printf("\t\t\t\t\t libcallbackapi fs seek start addr:0x%x,end addr:0x%x,sekRet:0x%x\n", dwSeekWay, dwSeekAddr, dwRet);
	return dwRet;
}

//���ļ��ж�ȡ����
//��ƽ̨��ʵ�ʲ���API,ʵ�ִ�function,lib�л���ô�function�������ļ����ݣ�
//  FS_HANDLE pFile 	/* [IN] File object */
//  void* buff,  		/* [OUT] Buffer to store read data */
//  u32 dwReadLen,    /* [IN] Number of bytes to read */
//  u32 *dwBr     	/* [OUT] Number of bytes read */
//����ֵ 1�ɹ���0ʧ��
WORD FS_Read(FS_HANDLE pFile,BYTE *pbReadBuf, u32 dwReadLen,u32 *dwBr)
{
	WORD ret = 1;

	*dwBr = fs_read(pFile,pbReadBuf,dwReadLen);
	//printf("\t\t\t\t\t libcallbackapi fs read len:0x%x,result :0x%x\n",dwReadLen,*dwBr);

	return ret;
}

//�����ļ�����
DWORD FS_GetFileLen(FS_HANDLE pFile)
{
	DWORD f_size = 0;

    fs_io_ctrl(NULL, pFile, FS_IO_GET_FILE_SIZE, &f_size);

	return f_size ;
}

