
#ifndef __EANBEL_DEBUG_DEF__
#define __EANBEL_DEBUG_DEF__

//常数定义
//-----------------------------------------------------------------------------------------------------------------------------------------
//Debug IO assign
#define SFR_PxFuncTimeM		p3m//SFR_P4M
#define SFR_PxFuncTimePH	p3ph//SFR_P4PH
#define SFR_PxFuncTimeEn	p3en//SFR_P4En
#define SFR_PxFuncTime		p3//SFR_P4

#define IO_DEBUG_FUNCTIME	11	//

//IO assign

#if 1
#define SFR_PxDebugM		p3m//SFR_P4M
#define SFR_PxDebugPH		p3ph//SFR_P4PH
#define SFR_PxDebugEn		p3en//SFR_P4En
#define SFR_PxDebug			p3//SFR_P4
#endif
#define IO_DEBUG_TOG		12	//IO,
//====================================================

//select Debug function
#define _DEBUG_MAIN         0   // 1 USE,0 NO ,debug ,test main time,tog

#define _DEBUG_TEST_TIME	1	//
#define _DEBUG_SAMLL_LOOP	0	//speed down ,small loop time test
#define _DEBUG_FALL_RASE_TIME	0	//
#define _DEBUG_32K_INT_TIME	0	//
#define _DEBUG_INIT_TIME	0	//main all init time
#define _DEBUG_PREFAT_TIME	0	//main all init time
#define _DEBUG_OPENDIR_TIME	0	//OPEN DIR TIME
#define _DEBUG_DECHDR_TIME	0	//dec bnf header TIME
#define _DEBUG_DECMP3_TIME	0	//dec bnf mp3 data TIME
#define _DEBUG_OPENBNF_TIME	0	//dec bnf mp3 data TIME
#define _DEBUG_SLEEPTO48MHZ_TIME	0	//从sleep mode wake time
#define _DEBUG_DEC_DATA_TIME		0	//从sleep mode wake time
#define _DEBUG_AES_DEC_TIME	0	//从sleep mode wake time




//-----------------------------------------------------------------------------------------------------------------------------------------


#endif//#ifndef __EANBEL_FUNC_DEF__
