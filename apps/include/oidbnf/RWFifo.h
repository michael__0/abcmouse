
#ifndef __RWFIFO__
#define	__RWFIFO__

#include "OidIndexSoundFunctionLib.h"

//类型定义
//typedef unsigned short    	WORD;          	// Unsigned 16 bit quantity
//typedef u32    	u32;          	// Unsigned 16 bit quantity
//类型定义
//本system中用到的ap code的值定义
typedef struct FIFOMessageTag{
    WORD uiApcType;            	//Fifo ap code type
    WORD uiSubType;            	//Fifo ap code sbu type
	u32 ulData1;				//Fifo data1 dword
	u32 ulData2;				//Fifo data2 dword

}MSG;
/*
//max for receive index 
typedef struct TagFifoBleTxItem{
    WORD uiCodeType;            //Fifo Tx数据类型，
	u32 ulData1;				//Fifo data1 dword
	u32 ulData2;				//Fifo data2 dword

}OIDTXITEM;
*/
//ble tx data form,与ap code保持一致for要送出除index之外的data,方便
//多几个w ram
typedef MSG OIDTXITEM;

//

//常数定义

#define MAX_FIFO_LED_CTRL 			1///2	//led fifo max define
#define MAX_FIFO_SYS_SOUND 			1///3	//system sound fifo max define
#define MAX_FIFO_APP 				1///8	//app fifo max define
#define MAX_FIFO_BLE_TX				1///3	//BLE Tx fifo max define

//Ap Code Type定义
#define APC_TYPE_NONE 				0	//表示无动作
#define APC_TYPE_KEY				1	//表示为按键，		用uiSubType表示键值
#define APC_TYPE_LED_CTRL			2	//表示LED控制相关
#define APC_TYPE_USB_CTRL			3	//表示USB控制相关，		用uiSubType表示 in /out 等
#define APC_TYPE_PWR_CTRL			4	//表示power控制相关，	用uiSubType表示 on/off/sleep,speed down/up等
#define APC_TYPE_AMP_CTRL			5	//表示AMP控制相关，		用uiSubType表示 on/off
#define APC_TYPE_LVD_CTRL			6	//表示系统电压相关，	用uiSubType表示 当前电量等级等
#define APC_TYPE_TIMER				7	//表示timer timeout,	用uiSubType表示	1s,3/6min等
#define APC_TYPE_DECODER			8	//表示decoder控制相关，	用uiSubType表示 reset,off等
#define APC_TYPE_BLE_CTRL			9	//表示ble控制相关		用uiSubType表示 on /off等
#define APC_TYPE_BLE_RX				10	//表示BLE收到的数据		用uiSubType表示 收到的数据类型等
#define APC_TYPE_USB				11	//
#define APC_TYPE_LCD				12	//表示LCD控制相关		用uiSubType表示 on/off,show volumn,show bnf name等
#define APC_TYPE_SD					13	//表示SD控制相关		用uiSubType表示 in /out，on/off等
#define APC_TYPE_RX_INDEX			14	//表示从decoder新收到的index	用uiSubType表示 indexFlag,ulData1,ulData2分别为收到的数据
#define APC_TYPE_VALID_INDEX		15	//表示从decoder收到的为有效index 用uiSubType表示 OID1.5/2/3/Position,ulData1,ulData2为相应值
#define APC_TYPE_INDEX_TYPE			16	//表示有效index解出来的类型		用uiSubType表示 indexType如book code,element等，ulData1为实际码值,ulData2为logic值或addr等，看type是什么决定
#define APC_TYPE_TX_INDEX			17	//表示要将index送出蓝牙或其它	用uiSubType表示 OID1.5/2/3/Position,ulData1,ulData2为相应值
#define APC_TYPE_MP3_SYS			18	//表示此要播放的声音号为systemSetting中的		用uiSubType表示 要播放的声音号(其中bit15 = 0,bit14-bit0为声音号)
#define APC_TYPE_MP3_ELEMENT		19	//表示此要播放的声音号为bnf中非游戏的声音编号	用ulData1表示 要播放的声音号 (因是dword)
#define APC_TYPE_MP3_SPI			20	//表示此要播放的声音号为spi中的声音编号			用uiSubType表示 要播放的声音号(其中bit15 = 1,bit14-bit0为声音号)
#define APC_TYPE_MP3_GAME			21	//表示此要播放的声音号为bnf中游戏的声音编号		用uiSubType表示 是localGameSound中的声音，还是globalGamesound,用ulData1表示 要播放的声音号 (因是dword)
#define APC_TYPE_MP3_PLAY			22	//表示启动MP3播放								用uiSubType表示 是systemSetting/spi/element/game,ulData1为startAddr,ulData2为endAddr
#define APC_TYPE_WAV				23	//表示启动wav播放
#define APC_TYPE_MODE_CHG			24	//表示模式转换,要重新load dlo	用uiSubType表示 目录dlo
#define APC_TYPE_VM					25	//表示音量调整					用uiSubType表示 +/-/loop/setlevel,或是setlevel ulData2为levelValue
#define APC_TYPE_SPEED				26	//表示音速调整					用uiSubType表示 +/-/loop/setlevel,或是setlevel ulData2为levelValue
#define APC_TYPE_PLAY_CTRL			27	//表示播放控制，				用uiSubType表示 puase/stop/play
#define APC_TYPE_ERR_CODE			28	//表示error
#define APC_TYPE_SYS_STATUS			29	//表示error
#define APC_TYPE_BLE_CMD			30	//表示BLE CTRL CMD
#define APC_TYPE_TONE				31	//表示要送tonecode
#define APC_TYPE_MDI_PLAY_CTRL		32	//表示播放控制，				用uiSubType表示 puase/stop/play

//用于表示 APC_TYPE_USB_CTRL 时，uiSubType值
#define SUB_USBCTRL_USBOUT			0	//usb拔出
#define SUB_USBCTRL_USBIN			1	//usb插入

//用于表示 APC_TYPE_PWR_CTRL 时，uiSubType值
#define SUB_PWR_ON					1	//
#define SUB_PWR_OFF					2	//power off right now
#define SUB_PWR_WAIT_OFF			3	//wait sound play over,then power off
#define SUB_PWR_SPEED_DOWN			4	//
#define SUB_PWR_SPEED_UP			5	//
#define SUB_PWR_SLEEP				6	//

//用于表示 APC_TYPE_LVD_CTRL 时，uiSubType值
#define SUB_LVD_LEVEL_0				0	//battery level,0关机，
#define SUB_LVD_LEVEL_1				1	//1及以上可用于提示或显示电量
#define SUB_LVD_LEVEL_2				2	//
#define SUB_LVD_LEVEL_3				3	//
#define SUB_LVD_LEVEL_4				4	//

//用于表示 APC_TYPE_VALID_INDEX及APC_TYPE_TX_INDEX 时，uiSubType值
#define SUB_TX_INDEX_OID15			1	//传送的index出来的类型-1.5代码
#define SUB_TX_INDEX_OID2			2	//传送的index出来的类型
#define SUB_TX_INDEX_OID3			3	//传送的index出来的类型
#define SUB_TX_INDEX_OID_POS		4	//传送的index出来的类型-position code
#define SUB_TX_ERR_CODE				5	//传送的一些出错码

//用于表示 APC_TYPE_MP3_PLAY及 时，uiSubType值
#define SUB_MP3_PLAY_SYS			1	//是systemSetting
#define SUB_MP3_PLAY_ELEMENT		2	//非Game
#define SUB_MP3_PLAY_GAME			3	//Game
#define SUB_MP3_PLAY_SPI			4	//spi
#define SUB_MP3_PLAY_DIC			5	//pPlayFile file play
#define SUB_MP3_PLAY_OTHER			6	//,pPlayFile file play

//用于表示是game中声音时，声音号是gamelocal的还是game global的以取得正常的start&end addr
#define SUB_MP3_GAME_LOCAL			0	//
#define SUB_MP3_GAME_GLOBAL			1	//

//用于表示 APC_TYPE_VM 时，uiSubType值
#define SUB_VM_UP					0	//音量加
#define SUB_VM_DOWN					1	//音量减
#define SUB_VM_SET_LEV				2	//直接设定音量级别
#define SUB_VM_LOOP					3	//音量循环

//用于表示 APC_TYPE_SPEED 时，uiSubType值
#define SUB_SPEED_UP				0	//速度加
#define SUB_SPEED_DOWN				1	//速度减
#define SUB_SPEED_SET_LEV			2	//直接设定速度级别
#define SUB_SPEED_LOOP				3	//速度循环

//用于表示 APC_TYPE_PLAY_CTRL 时，uiSubType值
#define SUB_PLAY_CTRL_PAUSE				0	//暂停
#define SUB_PLAY_CTRL_PLAY				1	//播放
#define SUB_PLAY_CTRL_STOP				2	//停止
#define SUB_PLAY_CTRL_NEXT				3	//下一首
#define SUB_PLAY_CTRL_PREV				4	//下一首
#define SUB_PLAY_CTRL_ACTIVE			5	//当前一首
#define SUB_PLAY_CTRL_FASTFORWARD		6	//快进
#define SUB_PLAY_CTRL_FASTBACKWARD		7	//快退

//用于表示 APC_TYPE_AMP_CTRL 时，uiSubType值
#define SUB_AMP_ON					0	//打开功放
#define SUB_AMP_OFF					1	//关闭功放

//用于表示APC_TYPE_TIMER时，uiSubType值
#define SUB_TIME_OUT1S				0	//表示1s timeout
#define SUB_TIME_OUT50MS			1	//表示50ms timeout
#define SUB_TIME_OUT3MIN			2	//表示3min timeout
#define SUB_TIME_OUTAUTOPWRDOWN		3	//表示自动关机 timeout

//用于表示ledfifo中值，对led控制值
#define SUB_LED_ONOFF				0	//

//用于表示 APC_TYPE_BLE_CTRL 时，uiSubType值
#define SUB_BLE_OFF					0	//关闭ble
#define SUB_BLE_ON					1	//打开ble
#define SUB_BLE_SET_24GMODE			2	//打开ble
#define SUB_BLE_SET_BLEMODE			3	//打开ble
#define SUB_BLE_SET_HIDMODE			4	//打开ble

//用于表示 APC_TYPE_DECODER 时，uiSubType值
#define SUB_DECODER_OFF				0	//关闭ble
#define SUB_DECODER_ON				1	//打开ble
#define SUB_DECODER_RST				2	//打开ble

//用于表示 APC_TYPE_SD 时，uiSubType值
#define SUB_SD_OFF				0	//关闭ble
#define SUB_SD_ON				1	//打开ble
#define SUB_SD_RST				2	//打开ble
#define SUB_SD_INSERT			3	//sd card insert
#define SUB_SD_REMOVE			4	//sd card remove

//用于表示 APC_TYPE_LED_CTRL 时，uiSubType值
#define SUB_LED_CTRL_OFF			0	//关闭
#define SUB_LED_CTRL_ON				1	//打开
#define SUB_LED_CTRL_RST			2	//
#define SUB_LED_CTRL_FLASH_1S		3	//闪
#define SUB_LED_CTRL_FLASH_500MS	4	//闪 CYCLE = 500MS



#define LED_CTRL_TIME_OUT_ON		0	//led fifo数据，表示要亮led
#define LED_CTRL_TIME_OUT_OFF		1	//led fifo数据，表示要灭led

//用于表示 APC_TYPE_MDI_PLAY_CTRL 时，uiSubType值
#define SUB_MIDI_BACK_START				0	//播放
#define SUB_MIDI_BACK_STOP				1	//停止
#define SUB_MIDI_BACK_PAUSE				2	//暂停

//变量定义

//led ctrl fifo
//动作过步频繁，单独处置，以免影响正常动作执行
extern WORD g_uiFifoLedCtrl[MAX_FIFO_LED_CTRL];
extern WORD g_uiFifoLedCtrlRdPos;	//read pointer
extern WORD g_uiFifoLedCtrlWrPos;	//write pointer


//系统声音fifo
extern WORD g_uiFifoSysSound[MAX_FIFO_SYS_SOUND];	//系统提示音播放fifo,含tf 及spi中的声音，bit15 = 0 表示tf,bit15 = 1spi
extern WORD g_uiFifoSysSoundRdPos;	//read pointer
extern WORD g_uiFifoSysSoundWrPos;	//write pointer


//app fifo
extern MSG g_msgFIFOApp[MAX_FIFO_APP];
extern MSG g_msgApp;
extern WORD g_wFifoAppRdPos;	//read pointer
extern WORD g_wFifoAppWrPos;	//write pointer

//ble tx fifo
extern OIDTXITEM g_txFifoBleTx[MAX_FIFO_BLE_TX];
extern WORD g_wFifoBleTxRdPos;	//read pointer
extern WORD g_wFifoBleTxWrPos;	//write pointer


//接口函数定义
extern WORD GetLedCtrlFifo(WORD *uiSysSound);
extern void SetLedCtrlFifo(WORD uiSysSound);

extern WORD GetSysSoundFifo(WORD *uiSysSound);
extern void SetSysSoundFifo(WORD uiSysSound);
void SetSysSoundFifoAndPlay(WORD uiSysSound);

extern void SetAppFifo(MSG msg);
extern WORD GetAppFifo(MSG *msg);

extern void SetBleTxFifo(OIDTXITEM txItem);
extern WORD GetBleTxFifo(OIDTXITEM *txItem);

#endif
