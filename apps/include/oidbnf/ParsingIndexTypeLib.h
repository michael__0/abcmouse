
#ifndef __OID_PARSING__
#define __OID_PARSING__

#include "OidIndexSoundFunctionLib.h"
#include "IndexFunctionCodeDef.h"		//控制码顺序表文件
#include "RWFifo.h"


//常数定义
//#define FALSE				0
//#define TRUE				1

#define MAX_INDEX_DEBOUNCE_TIME 50//200		//ms，对收到的码作deoubnce的时间

//解析收到的ValidIindex，看其属于那种功能码，并返回功能码类型，
//功能码类型定义如下。
#define Idx_NONE 			0			//无效码，没有定义对应任何功能
#define Idx_BOOK 			1			//书码
#define Idx_ELEMENT 		2			//内容码
#define Idx_SCENARIO 		3			//场景码
#define Idx_FUNC 			4			//控制码
#define Idx_VM 				5			//音量码
#define Idx_CHAR			6			//字符码
#define Idx_GAME			7			//游戏码
#define Idx_CMD				8			//命令,开关机码
#define Idx_SPEED			9			//速度
#define Idx_IDLE			10			//idle code
#define Idx_BOOKOID2	 	11			//为组合书码
#define Idx_ShengYunMu 		12			//声母。
#define Idx_YunMu 			13			//韵母。
#define Idx_YinDiao 		14			//音调。
#define Idx_OpenApp			15			//open app code
#define Idx_HandWrite		16			//手写
#define Idx_HandWriteMem	17			//
#define Idx_PositionCode	18			//position code
#define Idx_INVALIDINDEX	19			//不是当前书本中的有效码
#define Idx_NeedSelectBook 	20			//未选书码就点码。
#define Idx_CARDBOOK		21			//卡片码
#define Idx_DEL_WAV			22			//删除码
#define Idx_RECORD			23			//录音码
#define Idx_LayerCode		30			//
#define Idx_DIYRecord		31			//DIY录音码
#define Idx_DIYRecordPLAY	32			//DIY录音播放码




#define REC_INDEX_RECORD	1			//删除码
#define REC_INDEX_RECPLAY	2			//速度
#define REC_INDEX_RECPLAY2	3			//速度

//变量定义
extern WORD  g_uiIndexDebounceTime; 	//确认书码的时间，此时间内不点则确认为书码或清除本次读取
extern u32 g_dwIndexSoundNoTemp; 		//ElementIndex sound number temp,for get sound group,for repeat...
extern u32 g_dwPhysicalIndexValue;	//Valid index Physical Value
extern const u32 Tb_CardBookRang[];	//卡片范围定义
extern WORD  g_wIgnoreIndexTime; 		//多长时间内不收码



//接口函数定义

//-----------------------------------------------------------------------------------------------------------------------------------------
//Function: 
//	GetValidIndex
//Description:
//	查看fifo中是否有新收到的index,并根据bit ctrl flag检查是否是有效的码值
//Input:    
//	MSG 	：appfifo中的index
//OutPut:   
//	MSG 	：将结果写入appfifo中的index
//Return:   
//	no
//Others:   
//	
//-----------------------------------------------------------------------------------------------------------------------------------------
extern void GetValidIndex(void);



//-----------------------------------------------------------------------------------------------------------------------------------------
//Function: 
//	GetIndexType
//Description:
//	将接收到的ValidIndex解成不同功能码类型，并转换成在该功能码段内的logic值，各功能段的logic值从0开始。但并不破坏原始数据值。
//Input:    
//	FS_HANDLE pFile 					：book file handle
//	FS_HANDLE pSystemFile			: system file handle
//	u32 dwOidValidCode	：index physical value,
//OutPut:   
//	WORD *puiIndexType				:index type value
//	u32 *dwIndexLogic		:index in type's range logic value
//Return:   
//	0 : 没有解到相应类型
//	1 ：有解到其中某中类型，相应的类型会被送到app fifo中
//Others:   
//	
//-----------------------------------------------------------------------------------------------------------------------------------------
WORD GetIndexType(FS_HANDLE pFile, FS_HANDLE pSystemFile, u32 dwOidValidCode,WORD *puiIndexType,u32 *dwIndexLogic);

//-----------------------------------------------------------------------------------------------------------------------------------------
//Function: 
//	GetBookCode
//Description:
//	看所点的书是否是书本码
//Input:    
//	u32 ulOIDDataValue : 要查看的index
//OutPut:   
//	MSG 	：将结果写入appfifo中
//Return:   
//	0 : 没有解到相应类型
//	1 ：有解到其中某中类型，相应的类型会被送到app fifo中
//Others:   
//	
//-----------------------------------------------------------------------------------------------------------------------------------------
extern u16 GetBookCode( u32 uOIDDataLowWord);

//-----------------------------------------------------------------------------------------------------------------------------------------
//Function: 
//	GetCtrlCodeFromSysFile
//Description:
//	看所点的书是否是<65536的,且记录在sys file中的书本码或功能码
//Input:    
//	u32 dwOIDIndex : 要查看的index
//OutPut:   
//	MSG 	：将结果写入appfifo中
//Return:   
//	0 : 没有解到相应类型
//	1 ：有解到其中某中类型，相应的类型会被送到app fifo中
//Others:   
//	
//-----------------------------------------------------------------------------------------------------------------------------------------
extern u16 GetCtrlCodeFromSysFile(FS_HANDLE pSystemFile, u32 dwOIDIndex);

//-----------------------------------------------------------------------------------------------------------------------------------------
//Function: 
//	GetCardBookCode
//Description:
//	看所点的码是否是卡片码,在使用前要先打开卡片支持功能
//Input:    
//	u32 dwOidIndex : 要查看的index
//OutPut:   
//	MSG 	：将结果写入appfifo中
//Return:   
//	0 : 没有解到相应类型
//	1 ：有解到其中某中类型，相应的类型会被送到app fifo中
//Others:   
//	
//-----------------------------------------------------------------------------------------------------------------------------------------
extern u16 GetCardBookCode( u32 dwOidIndex);



//-----------------------------------------------------------------------------------------------------------------------------------------
//Function: 
//	MnParsingIndexType
//Description:
//	将接收到的ValidIndex解成不同功能码类型，并转换成在该功能码段内的logic值，各功能段的logic值从0开始。但并不破坏原始数据值。
//Input:    
//	FS_HANDLE pFile 			：book file handle
//	FS_HANDLE pSystemFile	: system file handle
//	MSG msgInput			：index值,
//OutPut:   
//	indexType,set to appfifo
//Return:   
//	0 : 没有解到相应类型
//	1 ：有解到其中某中类型，相应的类型会被送到app fifo中
//Others:   
//	
//-----------------------------------------------------------------------------------------------------------------------------------------
extern WORD MnParsingIndexType(FS_HANDLE pFile, FS_HANDLE pSystemFile, MSG msgInput);


#endif
