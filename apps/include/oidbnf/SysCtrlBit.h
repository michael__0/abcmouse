#ifndef __SYS_CTRL_BIT_H__
#define __SYS_CTRL_BIT_H__


#include "OidIndexSoundFunctionLib.h"

typedef union {
	u16 wValue;
	struct {
		u16 isNoRstIdleCnter:1;		//is reset idle time counter
		u16 isHavedRstDecoder:1;		//is haved reset decoder
		u16 isNewVerBook:1;			//is haved reset decoder
		u16 isNoBnfFile:1;			//is haved reset decoder
		u16 isTfOut:1;				//is haved detect tf out,just for one time sound
		u16 isOpenBooking:1;		//is haved detect tf out,just for one time sound
		u16 isT1Int:1;				//is t1 interrupt  
		u16 isPlayBackMidi:1;				//is t1 interrupt  
		u16 isGameShuShu:1;					//is abc mouse 数数游戏
		u16 isGameSpellWord:1;				//is 拼字游戏，答错要从头开始答
		u16 isGameSpecial440022:1;			//is 第一题的已答过答案作为第二题的特殊错答案
		u16 isRecPlayNoFile:1;				//is 
		u16 isFindMaxBokFile:1;				//is 
		u16 isSpcialAnsAddSnd:1;			//is //点到特殊答案中的前两个，或是一个特殊一个正确的，加播放一个特别音，如果点到的最后答案是正确答案，加播的声音不需要了
		u16 isAlwayCanAnsNoExit:1;			//is 答案一直可答，答完也不退出
		u16 isPassQuesExit:1;				//is 答对一题是否退出
	} Bit;
} BITCTRLITEM;

typedef union ctrlbit{
	u16 wValue;
	struct {
		u16 isPlayRepeatXSound:1;		//is 播放重复答案音时，是否找当前答到的第几个答案的第几个
		u16 isNormalErrIdxExit:1;		//is 点到普通错误答案是否退出
		u16 isPassSndByAnsOrder:1;		//is 问题通过音播放，要看所回答案的答案顺序，目前只是2个答案的，超过要再修改
		u16 isGameCtrlHadSeted:1;		//is 游戏设定已设for abc mouse特别处理的游戏，已设好，不需重设，for 已编好的数据不需重新编辑也可直接用
		u16 isLed1Off:1;				//is led1是否不亮
		u16 isBleTurnOn:1;				//is ble是否有打开
		u16 isStopSdMulti:1;			//is 是否有关闭sd multi transfor
		u16 isNeedResetOldBook:1;		//is 
		u16 :0;							//is 
	} Bit;
} BITCTRLITEM2;

#define PLAY_RST_IDLE_CNTER			0 
#define PLAY_NOT_RST_IDLE_CNTER		1 
#define DECODER_HAVED_NOT_RST		0 
#define DECODER_HAVED_RST			1 

#define TF_HAVE_BNF					0 
#define TF_NO_BNF					1 
#define TF_HAVE_NOT_OUT				0 
#define TF_HAVE_OUT					1 

#define OPEN_BOOK_ING_NO			0 
#define OPEN_BOOK_ING_YES			1 

#define T1_INT_NO					0 
#define T1_INT_YES					1 

#define PLAY_BACK_MIDI_NO			0 
#define PLAY_BACK_MIDI_YES			1 

#define GAME_SHU_SHU_NO				0 
#define GAME_SHU_SHU_YES			1 

#define GAME_SPELL_WORD_NO			0 
#define GAME_SPELL_WORD_YES			1 

#define GAME_SPECIAL440022_NO			0 
#define GAME_SPECIAL440022_YES			1 
#define RECCARDRECFILENOTEXIS_NO		0 
#define RECCARDRECFILENOTEXIS_YES		1 
#define FIND_MAX_BOK_FILE_NO			0 
#define FIND_MAX_BOK_FILE_YES			1 
#define SPECIAL_ANS_ADD_SOUND_NO		0 
#define SPECIAL_ANS_ADD_SOUND_YES		1 

#define G_ALWAY_CAN_ANS_NOEXIT_NO		0 
#define G_ALWAY_CAN_ANS_NOEXIT_YES		1 

#define PASS_QUES_EXIT_NO				0 
#define PASS_QUES_EXIT_YES				1 

#define PLAY_REPEATE_XSOUND_NO			0 
#define PLAY_REPEATE_XSOUND_YES			1 

#define NORMAL_ERRIDX_EXIT_NO			0 
#define NORMAL_ERRIDX_EXIT_YES			1 

#define PASS_SND_BY_ANSORD_NO			0 
#define PASS_SND_BY_ANSORD_YES			1 
#define GAMECTRL_HADSETED_NO			0 
#define GAMECTRL_HADSETED_YES			1 

#define LED1_IS_OFF_NO					0 
#define LED1_IS_OFF_YES					1 
#define BLE_TURN_ON_NO					0 
#define BLE_TURN_ON_YES					1 
#define BIT_STOP_SD_MULTI_NO			0 
#define BIT_STOP_SD_MULTI_YES			1 
#define BIT_NEED_RST_OLDBOK_NO			0 
#define BIT_NEED_RST_OLDBOK_YES			1 

//bit0 0 - onoff key control
//bit7   - oid mode sound play or not,0 no,1 yes
#define BCF_BIT7_0	0XFF7F 
#define BCF_BIT7_1	0X0080 
#define BCF_BIT8_0	0XFEFF 
#define BCF_BIT8_1	0X0100 
#define BCF_BIT9_0	0XFDFF 	//repeat ctrl
#define BCF_BIT9_1	0X0200 
#define BCF_BIT10_0	0XFBFF 	//
#define BCF_BIT10_1	0X0400 
#define BCF_BIT11_0	0XF7FF 	//
#define BCF_BIT11_1	0X0800 
#define BCF_BIT12_0	0XEFFF 	//
#define BCF_BIT12_1	0X1000 
#define BCF_BIT13_0	0XDFFF 	//
#define BCF_BIT13_1	0X2000 
#define BCF_BIT14_0	0XBFFF 	//
#define BCF_BIT14_1	0X4000 
#define BCF_BIT15_0_OPENBOOK	0X7FFF 	//
#define BCF_BIT15_1_OPENBOOK	0X8000 
//bit 1, 用于ADC的AIN0与AIN1选择，=0 AIN0，=1 AIN1
#define FLAG_BIT0_AIN0_OR_AIN1 BCF_BIT10_1 //0X0002
//bit 2, 用于KEY 
#define FLAG_BIT2_KEY_DECT BCF_BIT11_1 //0X0004
//bit 3, 用于KEY 
extern u16 g_adcValidValue;
extern u32 g_u32RandomValue;

extern BITCTRLITEM g_bitctrlitemSys;			// 
extern BITCTRLITEM2 g_bitctrlitem2Sys;			//

#endif
