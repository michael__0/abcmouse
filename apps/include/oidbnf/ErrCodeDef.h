
#ifndef __EANBEL_ERR_CODE__
#define __EANBEL_ERR_CODE__

//常数定义
//-----------------------------------------------------------------------------------------------------------------------------------------
#define ERR_NONE	 				0	// 1 USE,0 NO
#define ERR_SYS_SOUND_NUM_INVALID	1	// 1 USE,0 NO
#define ERR_SYS_HANDLE_INVALID 		2	// 1 USE,0 NO,
#define ERR_NO_BOOK_FILE	 		3 	// 1 USE,0 NO,
#define ERR_BK_FILE_NON_EXIT		4	// 1 USE,0 NO,
#define ERR_BK_CHK_SUM				5 	// 1 USE,0 NO,
#define ERR_BK_INVALID_CHIP			6 	// 1 USE,0 NO,
#define ERR_BK_INVALID_CHIP_ID	 	7 	// 1 USE,0 NO,
#define ERR_BK_INVALID_EDIT_ID 		8 	// 1 USE,0 NO,
#define ERR_BK_INVALID_STAMP 		9 	// 1 USE,0 NO,
#define ERR_BK_SN_CHK_SUM	 		10 	// 1 USE,0 NO,
#define ERR_BK_INVALID_SN		 	11 	// 1 USE,0 NO
#define ERR_RD_HANDLE_INVALID 		12 	// 1 USE,0 NO,

//-----------------------------------------------------------------------------------------------------------------------------------------
//OpenBookFile返回值定义
#define ERR_OPENBNF_OK			0//  0 ：打开书本成功
#define ERR_OPENBNF_NOBNF_INTF	1//  1 ：TF卡没有数据，
#define ERR_OPENBNF_OPENFAIL	2//  2 ：打开文件不成功
#define ERR_OPENBNF_NOTHISBOOK	3//	 3 ：TF卡中没有此书码的书

//-----------------------------------------------------------------------------------------------------------------------------------------
// g_wLibDebugOutputCtrl  定义lib输出debug 信息
#define ERR_LIB_DEBUG_NONE				0//
#define ERR_LIB_DEBUG_INIT_BOK_FILE		1//InitBookFile输出debug信息
#define ERR_LIB_DEBUG_INIT_MP3DEC		2//InitBookFile输出debug信息


#endif//#ifndef __EANBEL_FUNC_DEF__
