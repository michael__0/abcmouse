#ifndef __TWO_WIRE_H__
#define __TWO_WIRE_H__

#include "typedef.h"
#include "common/common.h"
#include "file_operate.h"

#define tw_delay()      delay(100)//7.6us

#define TW_PORT			JL_PORTA

#define TW_SCK				1
#define TW_SDIO				2

#define LOW                 0
#define HIGH                1

#define tw_clk_out()    do{TW_PORT->DIR &= ~BIT(TW_SCK);TW_PORT->PU &= ~BIT(TW_SCK);}while(0)
#define tw_clk_in()     do{TW_PORT->DIR |=  BIT(TW_SCK);TW_PORT->PU |=  BIT(TW_SCK);TW_PORT->PD &= ~BIT(TW_SCK);}while(0)
#define tw_clk_h()      do{TW_PORT->OUT |=  BIT(TW_SCK);TW_PORT->DIR &=~BIT(TW_SCK);}while(0)
#define tw_clk_l()      do{TW_PORT->OUT &= ~BIT(TW_SCK);TW_PORT->DIR &=~BIT(TW_SCK);}while(0)


#define tw_data_out()   do{TW_PORT->DIR &= ~BIT(TW_SDIO);TW_PORT->PU &= ~BIT(TW_SDIO);}while(0)
#define tw_data_in()    do{TW_PORT->DIR |=  BIT(TW_SDIO);TW_PORT->PU |=  BIT(TW_SDIO);TW_PORT->PD &= ~BIT(TW_SDIO);}while(0)
#define tw_data_r()     (TW_PORT->IN&BIT(TW_SDIO))
#define tw_data_h()     do{TW_PORT->OUT |=  BIT(TW_SDIO);TW_PORT->DIR &= ~BIT(TW_SDIO);}while(0)
#define tw_data_l()     do{TW_PORT->OUT &= ~BIT(TW_SDIO);TW_PORT->DIR &= ~BIT(TW_SDIO);}while(0)



#define GPIO_HIGH       1
#define GPIO_LOW        0
#define BIT31       0x80000000
#define BIT30       0x40000000
#define BIT29       0x20000000
#define BIT28       0x10000000
#define BIT27       0x08000000
#define BIT26       0x04000000
#define BIT25       0x02000000
#define BIT24       0x01000000
#define BIT23       0x00800000
#define BIT22       0x00400000
#define BIT21       0x00200000
#define BIT20       0x00100000
#define BIT19       0x00080000
#define BIT18       0x00040000
#define BIT17       0x00020000
#define BIT16       0x00010000
#define BIT15       0x00008000
#define BIT14       0x00004000
#define BIT13       0x00002000
#define BIT12       0x00001000
#define BIT11       0x00000800
#define BIT10       0x00000400
#define BIT9        0x00000200
#define BIT8        0x00000100
#define BIT7        0x00000080
#define BIT6        0x00000040
#define BIT5        0x00000020
#define BIT4        0x00000010
#define BIT3        0x00000008
#define BIT2        0x00000004
#define BIT1        0x00000002
#define BIT0        0x00000001



//void master_setup_slave_init();
bool aio_init(void);
//u8 get_read_state(void);
u32 get_oidcode(void);
u32 read_s2_index(u32 data, FILE_OPERATE* obj);

#endif // __TWO_WIRE_H__
