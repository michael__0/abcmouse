#ifndef _RES_FILE_H_
#define	_RES_FILE_H_

#define RES_BT_CFG             	"bt_cfg.bin"
#define RES_HW_EQ_CFG			"eq_cfg.bin"
#define RES_SW_EQ_CFG         	"eq_cfg.bin"

#define RES_POWER_ON_MP3        "/system/pwr_on.***"
#define RES_POWER_OFF_MP3       "/system/pwr_off.***"
#define RES_OID_GAME_MP3        "/system/go.mp3"

#define RES_OID_MODE_MP3        "/system/oid.***"
#define RES_OID_RFBOOK_MP3      "/system/rfbook.***"
#define RES_OID_CNREAD_MP3      "/system/cnread.***"
#define RES_AYHERE_MP3       	"/system/here.***"

#define RES_VOL_UP_MP3          "/system/vol_up.***"
#define RES_VOL_DOWN_MP3        "/system/vol_dn.***"
#define RES_VOL_MAX_MP3         "/system/vol_max.***"
#define RES_VOL_MIN_MP3         "/system/vol_min.***"
#define RES_VOL_5               "/system/vol_5.***"
#define RES_VOL_4               "/system/vol_4.***"
#define RES_VOL_3               "/system/vol_3.***"
#define RES_VOL_2               "/system/vol_2.***"
#define RES_VOL_1               "/system/vol_1.***"
#define RES_SD_ERROR_MP3        "/tf_error.***"


#define RES_BT_MP3              "/bt.***"
#define RES_MUSIC_MP3           "/music.***"
#define RES_RADIO_MP3           "/radio.***"
#define RES_LINEIN_MP3          "/linein.***"
#define RES_RTC_MP3             "/rtc.***"
#define RES_ECHO_MP3            "/echo.***"
#define RES_REC_MP3             "/record.***"
#define RES_PC_MP3              "/pc.***"

#define RES_CONNECT_MP3         "/connect.***"
#define RES_DISCONNECT_MP3      "/disconnect.***"

#define RES_RING_MP3            "/ring.***"
#define RES_0_MP3               "/0.***"
#define RES_1_MP3               "/1.***"
#define RES_2_MP3               "/2.***"
#define RES_3_MP3               "/3.***"
#define RES_4_MP3               "/4.***"
#define RES_5_MP3               "/5.***"
#define RES_6_MP3               "/6.***"
#define RES_7_MP3               "/7.***"
#define RES_8_MP3               "/8.***"
#define RES_9_MP3               "/9.***"

#define RES_LOW_POWER_MP3       "/low_power.***"
#define RES_WARNING_MP3     	"/warning.***"
#define RES_TEST_MP3       		"/test.***"
#define RES_REC_DING            "/System/ding.mp3"
#define RES_REC_DONG            "/System/dong.mp3"

#endif
